<?php
/* @var $this TranspodersController */
/* @var $model DvbCOptions */
/* @var $form CActiveForm */
?>

<div class="form">


    <h2><?= Yii::t('menu', 'Опции DVB-C');?></h2>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'frequency'); ?>
        <?php echo $form->textField($model,'frequency'); ?>
        <?php echo $form->error($model,'frequency'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'symbolrate'); ?>
        <?php echo $form->textField($model,'symbolrate'); ?>
        <?php echo $form->error($model,'symbolrate'); ?>
    </div>

</div><!-- form -->