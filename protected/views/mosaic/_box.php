<?php
/* @var $this MosaicController */
/* @var $channel Channel */
/* @var $mayRefresh bool */

$css = <<<CSS
.mosaicBox {
display: inline-block;
padding: 4px;
margin: 2px;
width: 160px;
height: 160px;
text-align: center;
vertical-align: middle;
color: #000000;
}

.mosaicBoxUnknown {
background-color: #CCCCCC;
}

.mosaicBoxWorking {
background-color: #AACCAA;
}

.mosaicBoxWarning {
background-color: #CCCC00;
}

.mosaicBoxError {
background-color: #CC0000;
}

.mosaicTitle {
padding: 0;
margin: 2px;
}

.mosaicTimeLeft {
display: none;
}

.mosaicBoxState {
display: none;
}

.mosaicBoxUrl {
display: none;
}

.mosaicBoxForceUrl{
display: none;
}
CSS;

Yii::app()->clientScript->registerCss('mosaic', $css);

$js = <<<JS
function reloadMosaic()
{
    var timeLeft = 0;
    $( ".mosaicBox" ).each(
        function( index ) {
            timeLeft = $(this).find(".mosaicTimeLeft").text();
            if ($(this).find(".mosaicImage")[0].complete > 0 && timeLeft > 0)
            {
                timeLeft -= 1;
                $(this).find(".mosaicTimeLeft").text(timeLeft);
                if (timeLeft == 0)
                {
                    var div = this;
                    $.ajax({
                        url: $(this).find(".mosaicBoxUrl").text(),
                        cache: false,
                        success: function( html ) {
                          $(div).replaceWith( html);
                          sortMosaic();
                        }
                    });
                }
            }
        }
    );
    setTimeout(reloadMosaic, 1000);
};

sortMosaic();
reloadMosaic();
JS;

$forceReloadJs = <<<JS
function forceReload(img)
{
    var div = $(img).parent();
    var url = div.find(".mosaicBoxForceUrl").text();
    div.find('.mosaicImage').attr("src", "{PRELOAD}");
    if (div.find(".mosaicTimeLeft").text() > 0)
    {
        div.find('.mosaicTimeLeft').text(0);
        $.ajax({
            url: url,
            cache: false,
            success: function ( html ) {
                div.replaceWith( html );
                sortMosaic();
            }
        });
    }
}

function sortMosaic()
{
    $('div.mosaicBox').tsort( '.mosaicBoxState', {order:'desc'},'.mosaicTitle');
}
JS;

Yii::app()->clientScript->registerScript('mosaicJs', $js);
Yii::app()->clientScript->registerScript('mosaicJsForceReload', str_replace('{PRELOAD}', Yii::app()->assetManager->publish(Yii::app()->basePath.'/data/loading.png'),$forceReloadJs), CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->basePath.'/data/jquery.tinysort.min.js'));

$lastMonitoringEvent = $channel->lastMonitoringEvent;

$class = '';

if ($lastMonitoringEvent)
{
    switch ($lastMonitoringEvent->overallState)
    {
        case MonitoringEvent::STATE_WORKING:
            $class = 'mosaicBoxWorking';
            break;
        case MonitoringEvent::STATE_WARNING:
            $class = 'mosaicBoxWarning';
            break;
        case MonitoringEvent::STATE_ERROR:
        case MonitoringEvent::STATE_SCRAMBLED:
        case MonitoringEvent::STATE_FALLBACK:
            $class = 'mosaicBoxError';
            break;
        default:
        case MonitoringEvent::STATE_UNKNOWN:
            $class = 'mosaicBoxUnknown';
            break;
    }
}
else
{
    $class = 'mosaicBoxUnknown';
}

echo '<div class="mosaicBox '.$class.'">';

echo CHtml::image(
    $channel->getMosaicImageUrl($mayRefresh),
    $channel->name,
    array('width' => '160px', 'height' => '120px', 'class' => 'mosaicImage', 'onclick' => 'forceReload(this);')
);

echo CHtml::tag('p', array('class' => 'mosaicTitle'), $channel->name);
if ($lastMonitoringEvent && $lastMonitoringEvent->overallState != MonitoringEvent::STATE_WORKING)
    echo CHtml::tag('span', array(''), $lastMonitoringEvent->overallStateString);
echo CHtml::tag('span', array('class' => 'mosaicTimeLeft'), ($channel->lastMosaicImageUpdate ? rand(intval(Yii::app()->params['mosaicRefreshInterval'] / 4), intval(Yii::app()->params['mosaicRefreshInterval'])) : rand(3,10)));
echo CHtml::tag('span', array('class' => 'mosaicBoxUrl'), $this->createUrl('/mosaic/box', array('id' => $channel->id)));
echo CHtml::tag('span', array('class' => 'mosaicBoxState'), ($lastMonitoringEvent ? $lastMonitoringEvent->overallState : $lastMonitoringEvent));
echo CHtml::tag('span', array('class' => 'mosaicBoxForceUrl'), $this->createUrl('/mosaic/box', array('id' => $channel->id, 'forceRefresh' => 1)));
echo '</div>';
echo PHP_EOL, PHP_EOL;