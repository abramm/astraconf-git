<?php


class AstraConfigNewcamd {

    public $name;
    public $comment;
    public $host;
    public $port;
    public $user;
    public $pass;
    public $key;
    public $disable_emm;

    public function __construct($name = null, $comment = null, $host = null, $port = null, $user = null, $pass = null, $key = null, $disable_emm = null)
    {
        $this->name = $name;
        $this->comment = $comment;
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->pass = $pass;
        $this->key = $key;
        $this->disable_emm = $disable_emm;
    }

    public function asConfArray()
    {
        $retval = [];
        $retval['start'] = $this->name.' = newcamd({';
        $retval['comment'] = $this->comment;
        $retval['end'] = '})';
        $retval['content'] = [
            'name = "'.$this->name.'",',
            'host = "'.$this->host.'", port = '.$this->port.',',
            'user = "'.$this->user.'", pass = "'.$this->pass.'",',
            'key = "'.$this->key.'",',
            'disable_emm = '.($this->disable_emm ? 'true' : 'false').',',
        ];

        return $retval;
    }

}