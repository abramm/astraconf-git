<?php
/**
 * @var $this StreamsController
 * @var $model Stream
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список потоков') => array('/streams/index'),
);

$this->pageHeader = Yii::t('menu','Создание потока');
?>
<p>
    <?php
    $this->renderPartial('_form', array('model' => $model));
    ?>
</p>
