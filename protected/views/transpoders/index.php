<?php
/**
 * @var $this TranspodersController
 * @var $model DvbTranspoder
 */

$this->pageHeader = Yii::t('menu','Список трансподеров');
?>
<p>
    <?php
    $dataProvider=$model->search();

    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'columns' => array(
            'description',
            'satellite',
            'adapterLink:html',
            'type',
            array(
                'class'=>'CButtonColumn',
            ),
        ),
    ));

    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'create',
            'caption' => Yii::t('models', 'Создать'),
            'url' => array('create'),
        )
    );

    ?>
</p>
