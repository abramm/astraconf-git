<?php

class DatabaseCommand extends CConsoleCommand {

    public function actionDumpSchema() {
        $db = Yii::app()->db;
        /* @var $db CDbConnection */
        $matches = array();
        if (preg_match('/dbname=([^;$]*)/', $db->connectionString, $matches)) {
            $dbname = $matches[1];
        } else {
            return false;
        }
        if (preg_match('/host=([^;$]*)/', $db->connectionString, $matches)) {
            $host = $matches[1];
        } else {
            return false;
        }

        system("mysqldump -u " . $db->username . " -p" . $db->password . " " . $dbname . " -h " . $host . " --no-data| sed 's/ AUTO_INCREMENT=[0-9]*\b//' > " . Yii::app()->basePath . "/data/schema.mysql.sql");
        system("mysqldump -u " . $db->username . " -p" . $db->password . " " . $dbname . " -h " . $host . " tbl_migration >> " . Yii::app()->basePath . "/data/schema.mysql.sql");
    }

}

?>
