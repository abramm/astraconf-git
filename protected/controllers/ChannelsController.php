<?php

class ChannelsController extends Controller
{
    public function actionIndex($editMode = 0)
    {
        $channel = new Channel('search');
        $channel->unsetAttributes();
        if (isset($_REQUEST['Channel']))
            $channel->attributes = $_REQUEST['Channel'];
        $this->render('index', array('model' => $channel, 'editMode' => $editMode));
    }

    public function actionView($id)
    {
        $channel = self::loadModel(array('id' => $id));
        $this->render('view', array('model' => $channel));
    }

    public function actionUpdate($id)
    {
        $channel = self::loadModel(array('id' => $id));
        if (isset($_POST['Channel']))
        {
            $channel->attributes = $_POST['Channel'];
            $channel->save();
        }
        $this->render('update', array('model' => $channel));
    }

    public function actionCreate()
    {
        $channel = new Channel();
        $channel->num = $channel->dbConnection->createCommand('SELECT MAX(num) FROM `channel`;')->queryScalar() + 1;
        if (isset($_POST['Channel']))
        {
            $channel->attributes = $_POST['Channel'];
            if ($channel->save())
                $this->redirect(array('/channels/view', 'id' => $channel->id));
        }
        $this->render('create', array('model' => $channel));
    }

    public function actionDelete($id)
    {
        $channel = self::loadModel(array('id' => $id));
        if (!$channel->delete())
            throw new CHttpException(500);
        $this->redirect(Yii::app()->request->urlReferrer);
    }

    public function actionSwitchHaveContract($id)
    {
        $model = $this->loadModel(array('id' => $id));
        $model->haveContract = intval(!$model->haveContract);
        if (!$model->save())
            throw new CHttpException(500);
        echo $model->haveContractString;
        Yii::app()->end();
    }

    public function actionSwitchIsMandatory($id)
    {
        $model = $this->loadModel(array('id' => $id));
        $model->isMandatory = intval(!$model->isMandatory);
        if (!$model->save())
            throw new CHttpException(500);
        echo $model->isMandatoryString;
        Yii::app()->end();
    }

    public function actionSwitchIsLicensed($id)
    {
        $model = $this->loadModel(array('id' => $id));
        $model->isLicensed = intval(!$model->isLicensed);
        if (!$model->save())
            throw new CHttpException(500);
        echo $model->isLicensedString;
        Yii::app()->end();
    }

    /**
     * @param $options
     * @return Channel
     * @throws CHttpException
     */
    public static function loadModel($options)
    {
        $model = Channel::model()->findByAttributes($options);
        if (!$model)
            throw new CHttpException(404);
        return $model;
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated users to access all actions
                'users'=>array('@'),
            ),
            array('deny'),
        );
    }

}