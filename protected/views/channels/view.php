<?php
/**
 * @var $this ChannelsController
 * @var $model Channel
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список каналов') => array('/channels/index'),
);

$this->pageHeader = Yii::t('menu','Просмотр канала {NAME}', array('{NAME}' => $model->name));
?>
<p>
    <?php
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
            'id',
            'num',
            'name',
            'genreLabel',
            'sortPriority',
            'subscriptionLevel',
            'xmlId',
            'ssIptvId',
            'serverLink:html',
            'haveContractString:html',
            'isMandatoryString:html',
            'isLicensedString:html',
            'canCastString:html',
        ),
    ));

    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'update',
            'caption' => Yii::t('models', 'Редактировать'),
            'url' => array('update', 'id' => $model->id),
        )
    );
    ?>
</p>
