<?php
/**
 * @var $this SoftcamServersController
 * @var $model SoftcamServer
 */

$this->pageHeader = Yii::t('menu','Список серверов ключей');
?>
<p>
    <?php
//    $model->with('streamsCount');
    $dataProvider=$model->search();

    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'columns' => array(
            'name',
            'host',
            'port',
            'username',
            'password',
            array(
                'name' => 'streamsCount',
                'filter' => false,
            ),
            array(
                'name' => 'sendEMM',
                'filter' => SoftcamServer::getSendEMMList(),
                'value' => '$data->sendEMMLabel'
            ),
            array(
                'class'=>'CButtonColumn',
            ),
        ),
    ));

    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'create',
            'caption' => Yii::t('models', 'Создать'),
            'url' => array('create'),
        )
    );

    ?>
</p>
