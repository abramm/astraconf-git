<?php

class MonitoringController extends Controller {

    public function actionEvent()
    {
//        $server = Server::model()->findByAttributes(array('ip' => Yii::app()->request->userHostAddress));
//        /* @var $server Server */
//        if (!$server)
//            throw new CHttpException(403);
        $rawData = file_get_contents('php://input');
        $data = json_decode($rawData, true);
        if (!$data)
            throw new CException('Error decoding JSON event data');
        foreach($data as $row)
        {
            $event = new MonitoringEvent();
            $event->attributes = $row;
            $event->scrambled = intval($event->scrambled);
            $event->onair = intval($event->onair);
            $event->fallback = intval($event->fallback);
//            Yii::log(var_export($event->attributes, true), CLogger::LEVEL_ERROR);
            if (!$event->save())
                Yii::log('Monitoring event not saved, raw data was: '.PHP_EOL.'"'.$rawData.'"'.PHP_EOL.', row was: '.PHP_EOL.'"'.var_export($row, true).'"', CLogger::LEVEL_ERROR);
        }
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => 'event'),
            array('allow', // allow authenticated users to access all actions
                'users'=>array('@'),
            ),
            array('deny'),
        );
    }

}