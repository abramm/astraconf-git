<?php

class m131127_120930_drop_fk extends CDbMigration
{
    public function up()
    {
        $dbName = null;
        $dbName = $this->getDbConnection()->createCommand('SELECT DATABASE();')->queryScalar();
        if (!$dbName)
        {
            return false;
        }

        $this->execute("SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
");

        $listKeysQuery = "SELECT TABLE_NAME, CONSTRAINT_NAME
FROM information_schema.TABLE_CONSTRAINTS
WHERE CONSTRAINT_TYPE='FOREIGN KEY' AND TABLE_SCHEMA='".$dbName."';";
        $keysArray = $this->getDbConnection()->createCommand($listKeysQuery)->queryAll();
        foreach($keysArray as $row)
        {
            $this->execute('ALTER TABLE '.$row['TABLE_NAME'].' DROP FOREIGN KEY '.$row['CONSTRAINT_NAME'].';');
        }

        $this->execute("SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
");

    }

	public function down()
	{
		echo "m131127_120930_drop_fk does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}