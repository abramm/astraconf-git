<?php

class m131127_121057_recreate_fk extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `channel`
  ADD CONSTRAINT `fk_channel_server1`
  FOREIGN KEY (`serverId` )
  REFERENCES `server` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_channel_server1_idx` (`serverId` ASC) 
, DROP INDEX `fk_channel_server1_idx` ;

ALTER TABLE `stream`
  ADD CONSTRAINT `fk_stream_server`
  FOREIGN KEY (`serverId` )
  REFERENCES `server` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION, 
  ADD CONSTRAINT `fk_stream_channel1`
  FOREIGN KEY (`channelId` )
  REFERENCES `channel` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION, 
  ADD CONSTRAINT `fk_stream_dvbTranspoder1`
  FOREIGN KEY (`dvbTranspoderId` )
  REFERENCES `dvbTranspoder` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_stream_dvbTranspoder1_idx` (`dvbTranspoderId` ASC) 
, DROP INDEX `fk_stream_dvbTranspoder1_idx` ;

ALTER TABLE `adapter` 
  ADD CONSTRAINT `fk_adapter_server1`
  FOREIGN KEY (`serverId` )
  REFERENCES `server` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `dvbTranspoder` 
  ADD CONSTRAINT `fk_dvbTranspoder_adapter1`
  FOREIGN KEY (`adapterId` )
  REFERENCES `adapter` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `dvbSOptions` 
  ADD CONSTRAINT `fk_dvbSOptions_dvbTranspoder1`
  FOREIGN KEY (`dvbTranspoderId` )
  REFERENCES `dvbTranspoder` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `dvbTOptions` 
  ADD CONSTRAINT `fk_dvbTOptions_dvbTranspoder1`
  FOREIGN KEY (`dvbTranspoderId` )
  REFERENCES `dvbTranspoder` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `stream_has_softcamServer` 
  ADD CONSTRAINT `fk_stream_has_softcamServer_stream1`
  FOREIGN KEY (`stream_id` )
  REFERENCES `stream` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION, 
  ADD CONSTRAINT `fk_stream_has_softcamServer_softcamServer1`
  FOREIGN KEY (`softcamServer_id` )
  REFERENCES `softcamServer` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `monitoringEvent`
  ADD CONSTRAINT `fk_monitoringData_channel1`
  FOREIGN KEY (`channelId` )
  REFERENCES `channel` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION, 
  ADD CONSTRAINT `fk_monitoringEvent_stream1`
  FOREIGN KEY (`streamId` )
  REFERENCES `stream` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION, 
  ADD CONSTRAINT `fk_monitoringEvent_softcamServer1`
  FOREIGN KEY (`softcamServerId` )
  REFERENCES `softcamServer` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_monitoringData_channel1_idx` (`channelId` ASC) 
, ADD INDEX `fk_monitoringEvent_stream1_idx` (`streamId` ASC) 
, ADD INDEX `fk_monitoringEvent_softcamServer1_idx` (`softcamServerId` ASC) 
, DROP INDEX `fk_monitoringEvent_softcamServer1_idx` 
, DROP INDEX `fk_monitoringEvent_stream1_idx` 
, DROP INDEX `fk_monitoringData_channel1_idx` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

SQL;
        
        $this->execute($sql);

	}

	public function down()
	{
		echo "m131127_121057_recreate_fk does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}