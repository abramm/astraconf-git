<?php
/**
 * @var $this StreamsController
 * @var $model Stream
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список потоков') => array('/streams/index'),
);

$this->pageHeader = Yii::t('menu','Просмотр потока {NAME}', array('{NAME}' => $model->name));
?>
<p>
    <?php
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
            'name',
            'description',
            'serverLink:html',
            'channelLink:html',
            array(
                'name' => 'type',
                'value' => $model->typeLabel,
            ),
            'dvbTranspoderLink:html',
            array(
                'name' => 'isEncoded',
                'value' => $model->isEncodedLabel,
            ),
        ),
    ));

    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'update',
            'caption' => Yii::t('models', 'Редактировать'),
            'url' => array('update', 'id' => $model->id),
        )
    );

    ?>
</p>
<?php
if ($model->softcamServers > 0)
{
?>
<p><?= Yii::t('models','Привязаны следующие SoftCAM-серверы:'); ?></p>
<p>
    <?php
    $softCamSearch = new SoftcamServer('search');
    $softCamSearch->unsetAttributes();
    $softCamSearch->streamSearch = $model->id;

    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider' => $softCamSearch->search(),
        'columns' => array(
            'name',
            array(
                'class' => 'CButtonColumn',
                'viewButtonUrl' => 'Yii::app()->controller->createUrl("//softcamServers/view",array("id"=>$data->primaryKey))',
                'template' => '{view}',
            ),
        ),
    ))
    ?>
</p>
<?php
}