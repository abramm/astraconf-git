<?php

class m131126_125311_monitoring_autoIncrement extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
ALTER TABLE `monitoringEvent` CHANGE COLUMN `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;
SQL;
        $this->execute($sql);
	}

	public function down()
	{
		echo "m131126_125311_monitoring_autoIncrement does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}