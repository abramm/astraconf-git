<?php

class m140512_131932_keepHttpActive extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
ALTER TABLE `server` ADD COLUMN `keepHttpActive` INT(1) UNSIGNED NOT NULL DEFAULT 0  AFTER `enableUdpOutput`;
SQL;
        $this->execute($sql);

	}

	public function down()
	{
		echo "m140512_131932_keepHttpActive does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}