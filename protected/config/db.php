<?php

$dbConf = array(
    'components' => array(
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=astraconf',
            'emulatePrepare' => true,
            'username' => 'astraconf',
            'password' => 'b9qWwrMuMZ6TZmEX',
            'charset' => 'utf8',
            'enableProfiling' => YII_DEBUG,
            'enableParamLogging' => YII_DEBUG,
        ),
    ),
);

$userConf = require(dirname(__FILE__).'/../../config/db.php');
return CMap::mergeArray($dbConf, $userConf);