<?php
/**
 * @var $this ChannelsController
 * @var $model Channel
 * @var $editMode bool
 */

$this->pageHeader = Yii::t('menu','Список каналов');
?>
<p>
    <?php
    $dataProvider=$model->search();

    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'ajaxUpdate'=>false,
        'filter'=>$model,
        'columns'=>array(
            array(
                'name' => 'num',
                'filter' => false,
            ),
            'name',
            array(
                'name' => 'genre',
                'value' => '$data->genreLabel',
                'filter' => Channel::getGenresList(),
            ),
            'subscriptionLevel',
            array(
                'name' => 'haveContract',
                'filter' => MyActiveRecord::boolStringPossibleValues(),
                'value' => '$data->haveContractClickableString',
                'type' => 'raw',
                'visible' => $editMode,
            ),
            array(
                'name' => 'isMandatory',
                'filter' => MyActiveRecord::boolStringPossibleValues(),
                'value' => '$data->isMandatoryClickableString',
                'type' => 'raw',
                'visible' => $editMode,
            ),
            array(
                'name' => 'isLicensed',
                'filter' => MyActiveRecord::boolStringPossibleValues(),
                'value' => '$data->isLicensedClickableString',
                'type' => 'raw',
                'visible' => $editMode,
            ),
            array(
                'name' => 'canCastSearch',
                'filter' => MyActiveRecord::boolStringPossibleValues(),
                'value' => '$data->canCastString',
                'type' => 'html',
                'visible' => $editMode,
            ),
            array(
                'class'=>'CButtonColumn',
            ),
        )
    ));

    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'create',
            'caption' => Yii::t('models', 'Создать'),
            'url' => array('create'),
        )
    );

    if (!$editMode)
    {
        $this->widget('zii.widgets.jui.CJuiButton',array(
                'buttonType'=>'link',
                'name' => 'edit',
                'caption' => Yii::t('models', 'Режим редактирования'),
                'url' => array('/channels/index', 'editMode' => 1),
            )
        );
    }

    ?>
</p>
