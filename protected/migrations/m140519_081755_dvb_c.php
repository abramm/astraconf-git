<?php

class m140519_081755_dvb_c extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `dvbCOptions` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dvbTranspoderId` INT(10) UNSIGNED NOT NULL,
  `frequency` INT(10) UNSIGNED NOT NULL,
  `symbolrate` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_dvbCOptions_dvbTranspoder2_idx` (`dvbTranspoderId` ASC),
  CONSTRAINT `fk_dvbCOptions_dvbTranspoder2`
    FOREIGN KEY (`dvbTranspoderId`)
    REFERENCES `dvbTranspoder` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
SQL;
        $this->execute($sql);
	}

	public function down()
	{
		echo "m140519_081755_dvb_c does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}