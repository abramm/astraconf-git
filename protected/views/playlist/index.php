<?php
/**
 * @var $this PlaylistController
 */

$this->pageHeader = Yii::t('menu','Плейлисты');
$haveExternalIps = Server::model()->count('`externalIp` IS NOT NULL AND LENGTH(`externalIp`) > 0');
?>

<h3><?= Yii::t('models', 'M3U');?></h3>
<p>
<?php
$this->widget('zii.widgets.jui.CJuiButton',array(
        'buttonType'=>'link',
        'name' => 'view-m3u',
        'caption' => Yii::t('models', 'Показать'),
        'url' => array('view', 'type' => PlaylistController::PLAYLIST_M3U),
    )
);
$this->widget('zii.widgets.jui.CJuiButton',array(
        'buttonType'=>'link',
        'name' => 'download-m3u',
        'caption' => Yii::t('models', 'Скачать'),
        'url' => array('download', 'type' => PlaylistController::PLAYLIST_M3U),
    )
);
?>
</p>
<?php
if ($haveExternalIps)
{
?>
<p>
    <h4><?= Yii::t('models', 'Внешние IP');?></h4>
    <?php
    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'view-m3u-extIp',
            'caption' => Yii::t('models', 'Показать'),
            'url' => array('view', 'type' => PlaylistController::PLAYLIST_M3U, 'externalIp' => true),
        )
    );
    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'download-m3u-extIp',
            'caption' => Yii::t('models', 'Скачать'),
            'url' => array('download', 'type' => PlaylistController::PLAYLIST_M3U, 'externalIp' => true),
        )
    );
    ?>
</p>
<?php
}
?>
<h3><?= Yii::t('models', 'Stalker');?></h3>

<p>
<?php
$this->widget('zii.widgets.jui.CJuiButton',array(
        'buttonType'=>'link',
        'name' => 'view-stalker',
        'caption' => Yii::t('models', 'Показать'),
        'url' => array('view', 'type' => PlaylistController::PLAYLIST_STALKER),
    )
);
$this->widget('zii.widgets.jui.CJuiButton',array(
        'buttonType'=>'link',
        'name' => 'download-stalker',
        'caption' => Yii::t('models', 'Скачать'),
        'url' => array('download', 'type' => PlaylistController::PLAYLIST_STALKER),
    )
);
if (false)
$this->widget('zii.widgets.jui.CJuiButton',array(
        'buttonType'=>'link',
        'name' => 'upload-stalker',
        'caption' => Yii::t('models', 'Залить в Stalker'),
        'url' => array('upload', 'type' => PlaylistController::PLAYLIST_STALKER),
    )
);
?>
</p>
<?php
if ($haveExternalIps)
{
    ?>
    <p>
    <h4><?= Yii::t('models', 'Внешние IP');?></h4>
    <?php
    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'view-stalker-extIp',
            'caption' => Yii::t('models', 'Показать'),
            'url' => array('view', 'type' => PlaylistController::PLAYLIST_STALKER, 'externalIp' => true),
        )
    );
    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'download-stalker-extIp',
            'caption' => Yii::t('models', 'Скачать'),
            'url' => array('download', 'type' => PlaylistController::PLAYLIST_STALKER, 'externalIp' => true),
        )
    );
    if (false)
        $this->widget('zii.widgets.jui.CJuiButton',array(
                'buttonType'=>'link',
                'name' => 'upload-stalker-extIp',
                'caption' => Yii::t('models', 'Залить в Stalker'),
                'url' => array('upload', 'type' => PlaylistController::PLAYLIST_STALKER, 'externalIp' => true),
            )
        );
    ?>
    </p>
<?php
}
?>

<h3><?= Yii::t('models', 'Stalker (расширенный)');?></h3>
<p>
<?php
$this->widget('zii.widgets.jui.CJuiButton',array(
        'buttonType'=>'link',
        'name' => 'view-stalker-ext',
        'caption' => Yii::t('models', 'Показать'),
        'url' => array('view', 'type' => PlaylistController::PLAYLIST_STALKER_EXTENDED),
    )
);
$this->widget('zii.widgets.jui.CJuiButton',array(
        'buttonType'=>'link',
        'name' => 'download-stalker-ext',
        'caption' => Yii::t('models', 'Скачать'),
        'url' => array('download', 'type' => PlaylistController::PLAYLIST_STALKER_EXTENDED),
    )
);
if (false)
$this->widget('zii.widgets.jui.CJuiButton',array(
        'buttonType'=>'link',
        'name' => 'upload-stalker-ext',
        'caption' => Yii::t('models', 'Залить в Stalker'),
        'url' => array('upload', 'type' => PlaylistController::PLAYLIST_STALKER_EXTENDED),
    )
);
?>

</p>

<?php
if ($haveExternalIps)
{
    ?>
    <p>
    <h4><?= Yii::t('models', 'Внешние IP');?></h4>
    <?php
    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'view-stalker-ext-extIp',
            'caption' => Yii::t('models', 'Показать'),
            'url' => array('view', 'type' => PlaylistController::PLAYLIST_STALKER_EXTENDED, 'externalIp' => true),
        )
    );
    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'download-stalker-ext-extIp',
            'caption' => Yii::t('models', 'Скачать'),
            'url' => array('download', 'type' => PlaylistController::PLAYLIST_STALKER_EXTENDED, 'externalIp' => true),
        )
    );
    if (false)
        $this->widget('zii.widgets.jui.CJuiButton',array(
                'buttonType'=>'link',
                'name' => 'upload-stalker-ext-extIp',
                'caption' => Yii::t('models', 'Залить в Stalker'),
                'url' => array('upload', 'type' => PlaylistController::PLAYLIST_STALKER_EXTENDED, 'externalIp' => true),
            )
        );
    ?>
    </p>
<?php
}
?>