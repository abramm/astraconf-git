<?php
/* @var $this ChannelController */
/* @var $model Channel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'channel-_form-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note"><?= Yii::t('models', 'Поля с {*} обязательны к заполнению', array('{*}' => '<span class="required">*</span>')); ?></p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'serverId'); ?>
        <?php echo $form->dropDownList($model,'serverId',CHtml::listData(Server::model()->findAll(),'id', 'name')); ?>
        <?php echo $form->error($model,'serverId'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'num'); ?>
		<?php echo $form->textField($model,'num'); ?>
		<?php echo $form->error($model,'num'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'genre'); ?>
		<?php echo $form->dropDownList($model,'genre',Channel::getGenresList()); ?>
		<?php echo $form->error($model,'genre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sortPriority'); ?>
		<?php echo $form->textField($model,'sortPriority'); ?>
		<?php echo $form->error($model,'sortPriority'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subscriptionLevel'); ?>
		<?php echo $form->textField($model,'subscriptionLevel'); ?>
		<?php echo $form->error($model,'subscriptionLevel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ssIptvId'); ?>
		<?php echo $form->textField($model,'ssIptvId'); ?>
		<?php echo $form->error($model,'ssIptvId'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'haveContract'); ?>
        <?php echo $form->checkBox($model,'haveContract'); ?>
        <?php echo $form->error($model,'haveContract'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'isMandatory'); ?>
        <?php echo $form->checkBox($model,'isMandatory'); ?>
        <?php echo $form->error($model,'isMandatory'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'isLicensed'); ?>
        <?php echo $form->checkBox($model,'isLicensed'); ?>
        <?php echo $form->error($model,'isLicensed'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'xmlId'); ?>
        <?php echo $form->textField($model,'xmlId'); ?>
        <?php echo $form->error($model,'xmlId'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'epgCorrection'); ?>
        <?php echo $form->textField($model,'epgCorrection'); ?>
        <?php echo $form->error($model,'epgCorrection'); ?>
    </div>


	<div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('app', 'Сохранить')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->