<?php

/**
 * This is the model class for table "dvbSOptions".
 *
 * The followings are the available columns in table 'dvbSOptions':
 * @property string $id
 * @property string $dvbTranspoderId
 * @property string $frequency
 * @property string $polarization
 * @property string $symbolrate
 * @property string $lof1
 * @property string $lof2
 * @property string $slof
 * @property string $lnb_sharing
 * @property string $diseqc
 * @property string $tone
 * @property string $rolloff
 *
 * The followings are the available model relations:
 * @property DvbTranspoder $dvbTranspoder
 */
class DvbSOptions extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dvbSOptions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dvbTranspoderId, frequency, polarization, symbolrate, lof1, lof2, slof', 'required'),
			array('dvbTranspoderId, frequency, symbolrate, lof1, lof2, slof', 'length', 'max'=>10),
			array('polarization', 'length', 'max'=>4),
			array('lnb_sharing, diseqc, tone', 'length', 'max'=>1),
			array('rolloff', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dvbTranspoderId, frequency, polarization, symbolrate, lof1, lof2, slof, lnb_sharing, diseqc, tone, rolloff', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dvbTranspoder' => array(self::BELONGS_TO, 'DvbTranspoder', 'dvbTranspoderId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dvbTranspoderId' => Yii::t('models', 'Трансподер'),
			'frequency' => Yii::t('models', 'Частота несущей'),
			'polarization' => Yii::t('models', 'Поляризация'),
			'symbolrate' => Yii::t('models', 'Скорость потока'),
			'lof1' => Yii::t('models', 'Нижний поддиапазон частоты гетеродина (LOF1)'),
			'lof2' => Yii::t('models', 'Верхний поддиапазон частоты гетеродина (LOF2)'),
			'slof' => Yii::t('models', 'Граница выбора поддиапазона частоты гетеродина (SLOF)'),
			'lnb_sharing' => Yii::t('models', 'Отключить подачу напряжения на конвертор (LNB Sharing)'),
			'diseqc' => Yii::t('models', 'Номер входа DiSEqC, 0 - не использовать'),
			'tone' => Yii::t('models', 'Использовать тонально-импульсный сигнал'),
			'rolloff' => Yii::t('models', 'Эффективность использования спектра'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dvbTranspoderId',$this->dvbTranspoderId);
		$criteria->compare('frequency',$this->frequency,true);
		$criteria->compare('polarization',$this->polarization,true);
		$criteria->compare('symbolrate',$this->symbolrate,true);
		$criteria->compare('lof1',$this->lof1,true);
		$criteria->compare('lof2',$this->lof2,true);
		$criteria->compare('slof',$this->slof,true);
		$criteria->compare('lnb_sharing',$this->lnb_sharing,true);
		$criteria->compare('diseqc',$this->diseqc,true);
		$criteria->compare('tone',$this->tone,true);
		$criteria->compare('rolloff',$this->rolloff,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DvbSOptions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
