<?php

/**
 * This is the model class for table "softcamServer".
 *
 * The followings are the available columns in table 'softcamServer':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $host
 * @property string $port
 * @property string $username
 * @property string $password
 * @property string $aesKey
 * @property string $sendEMM
 * @property string $separateConnections
 *
 * The followings are the available model relations:
 * @property Stream[] $streams
 * @property int      $streamsCount
 * @property StreamHasSoftcamServer[] $streamRelations
 */
class SoftcamServer extends MyActiveRecord
{

    public $streamSearch;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'softcamServer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name, description', 'length', 'max'=>255),
			array('host, username, password, aesKey', 'length', 'max'=>45),
			array('port', 'length', 'max'=>10),
			array('sendEMM, separateConnections', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, host, port, username, password, aesKey, sendEMM, separateConnections', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'streams' => array(self::MANY_MANY, 'Stream', 'stream_has_softcamServer(softcamServer_id, stream_id)'),
            'streamsCount' => array(self::STAT, 'StreamHasSoftcamServer', 'softcamServer_id'),
            'streamRelations' => array(self::HAS_MANY, 'StreamHasSoftcamServer', 'softcamServer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('models', 'Имя'),
			'description' => Yii::t('models', 'Описание'),
			'host' => Yii::t('models', 'Хост'),
			'port' => Yii::t('models', 'Порт'),
			'username' => Yii::t('models', 'Пользователь'),
			'password' => Yii::t('models', 'Пароль'),
			'aesKey' => Yii::t('models', 'AES ключ'),
			'sendEMM' => Yii::t('models', 'Отправка EMM'),
            'separateConnections' => Yii::t('models', 'Раздельные подключения'),
            'streamsCount' => Yii::t('models', 'Потоки'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->with = 'streamsCount';

        $sort = new CSort('SoftcamServer');
        $sort->attributes = array(
            '*',
            'streamsCount' => array(
                'asc'  => '`c`.`count` ASC',
                'desc' => '`c`.`count` DESC',
            ),
        );

        if (array_key_exists('streamsCount',$sort->directions))
        {
            $criteria->alias = 'ss';
            $criteria->join  .= ' LEFT OUTER JOIN (SELECT softcamServer_id, COUNT(stream_id) as count FROM stream_has_softcamServer) c ON ss.id = c.softcamServer_id';

        }

        if ($this->streamSearch)
        {
            $criteria->alias = 'ss';
            $criteria->join .= ' LEFT OUTER JOIN stream_has_softcamServer shs ON ss.id = shs.softcamServer_id';
            $criteria->compare('shs.stream_id', $this->streamSearch);
        }

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('host',$this->host,true);
		$criteria->compare('port',$this->port,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('aesKey',$this->aesKey,true);
		$criteria->compare('sendEMM',$this->sendEMM);
        $criteria->compare('separateConnections',$this->separateConnections);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>$sort,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SoftcamServer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getSendEMMList()
    {
        return array(
            0=>Yii::t('models', 'Нет'),
            1=>Yii::t('models', 'Да'),
        );
    }

    public static function sendEMMLabel($id)
    {
        if (!is_numeric($id))
            return $id;
        $list = self::getSendEMMList();
        if (isset($list[$id]))
            return $list[$id];
        else
            return null;
    }

    public function getSendEMMLabel()
    {
        return self::sendEMMLabel($this->sendEMM);
    }

    public function delete()
    {
        $retval = false;
        $useTransaction = !$this->dbConnection->currentTransaction;
        if ($useTransaction)
            $transaction = $this->dbConnection->beginTransaction();
        try
        {
            foreach ($this->streamRelations as $relation)
            {
                if (!$relation->delete())
                    throw new CException('Error deleting relation '.$relation->id);
            }
            $retval = parent::delete();
            if ($useTransaction)
                $transaction->commit();
        }
        catch(Exception $e)
        {
            Yii::log('Error deleting channel '.$this->id.', caught exception: '.$e->getMessage(), CLogger::LEVEL_ERROR);
            if ($useTransaction)
                $transaction->rollback();
        }
        return $retval;
    }
}
