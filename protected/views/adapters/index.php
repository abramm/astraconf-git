<?php
/**
 * @var $this AdaptersController
 * @var $model Adapter
 */

$this->pageHeader = Yii::t('menu','Список DVB адаптеров');
?>
<p>
    <?php
    $dataProvider=$model->search();

    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'columns' => array(
            'id',
            array(
                'name' => 'serverId',
                'value' => '$data->serverLink',
                'type' => 'html',
                'filter' => CHtml::listData(Server::model()->findAll(), 'id', 'name'),
            ),
            'num',
            'device',
            'mac',
            'inUse',
            array(
                'class'=>'CButtonColumn',
            ),
        ),
    ));

    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'create',
            'caption' => Yii::t('models', 'Создать'),
            'url' => array('create'),
        )
    );

    ?>
</p>
