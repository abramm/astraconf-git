<?php
/**
 * @var $this ChannelsController
 * @var $model Channel
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список каналов') => array('/channels/index'),
);

$this->pageHeader = Yii::t('menu','Создание канала');
?>
<p>
    <?php
    $this->renderPartial('_form', array('model' => $model));
    ?>
</p>
