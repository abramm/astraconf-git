<?php

class AstraUrl {

    public $schema;
    public $host;
    public $port;
    public $path;
    public $params;
    public $udpInterface;

    public function __construct($schema = '', $host = '', $port = 0, $path = '', $params = [])
    {
        $this->schema = $schema;
        $this->host = $host;
        $this->port = $port;
        $this->path = $path;
        $this->params = $params;
    }

    public function asString()
    {
        $params = '';
        if (count($this->params) > 0)
            $params = '#' . implode('&', $this->params);
        $host = $this->host;
        if ($this->schema == 'udp' && $this->udpInterface)
            $host = $this->udpInterface.'@'.$host;
        return
            $this->schema . '://'.
            $host .
            ($this->port ? ':'.$this->port . '/' : '').
            $this->path .
            $params;
    }

    /**
     * @param $val
     * @return AstraUrl
     */
    public function setPathFromString($val)
    {
        if ($val[0] == '/')
            $val = mb_substr($val, 1);
        $this->path = $val;
        return $this;
    }

    /**
     * @param $val
     * @return AstraUrl
     */
    public function setParamsFromString($val)
    {
        $this->params = [];
        if ($val[0] == '#')
            $val = mb_substr($val, 1);
        $params = explode('&',$val);
        foreach ($params as $param)
        {
            $param = trim($param);
            if (strlen($param) > 0)
                $this->params[] = $param;
        }
        return $this;
    }

    /**
     * @param $val
     * @return AstraUrl
     */
    public function addParam($val)
    {
        $this->params[] = $val;
        return $this;
    }
    /**
     * @param $val
     * @return AstraUrl
     */
    public function setUdpInterface($val)
    {
        $this->udpInterface = $val;
        return $this;
    }

    /**
     * @param $url URL to parse
     * @return AstraUrl
     */
    public static function fromString($url)
    {
        $ret = new AstraUrl();
        $ret->schema = parse_url($url, PHP_URL_SCHEME);
        $ret->host = parse_url($url, PHP_URL_HOST);
        $ret->udpInterface = parse_url($url, PHP_URL_USER);
        $ret->port = parse_url($url, PHP_URL_PORT);
        $ret->setPathFromString(parse_url($url, PHP_URL_PATH));
        $ret->setParamsFromString(parse_url($url, PHP_URL_FRAGMENT));
        return $ret;
    }

}