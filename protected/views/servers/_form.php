<?php
/* @var $this ServerController */
/* @var $model Server */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'server-_form-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?= Yii::t('models', 'Поля с {*} обязательны к заполнению', array('{*}' => '<span class="required">*</span>')); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'ip'); ?>
        <?php echo $form->textField($model,'ip'); ?>
        <?php echo $form->error($model,'ip'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'enableHttpOutput'); ?>
        <?php echo $form->checkBox($model,'enableHttpOutput'); ?>
        <?php echo $form->error($model,'enableHttpOutput'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'keepHttpActive'); ?>
        <?php echo $form->checkBox($model,'keepHttpActive'); ?>
        <?php echo $form->error($model,'keepHttpActive'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'enableUdpOutput'); ?>
        <?php echo $form->checkBox($model,'enableUdpOutput'); ?>
        <?php echo $form->error($model,'enableUdpOutput'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'externalIp'); ?>
        <?php echo $form->textField($model,'externalIp'); ?>
        <?php echo $form->error($model,'externalIp'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'urlInputProcesses'); ?>
        <?php echo $form->textField($model,'urlInputProcesses'); ?>
        <?php echo $form->error($model,'urlInputProcesses'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('app', 'Сохранить')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->