<?php

class m131126_140422_monitoring_notnull extends CDbMigration
{
    public function up()
    {
        $sql = <<<SQL
ALTER TABLE `monitoringEvent`
CHANGE COLUMN `pes_error` `pes_error` INT(20) UNSIGNED NULL DEFAULT NULL,
CHANGE COLUMN `cc_error` `cc_error` INT(20) UNSIGNED NULL DEFAULT NULL,
CHANGE COLUMN `bitrate` `bitrate` INT(20) UNSIGNED NULL DEFAULT NULL
SQL;
        $this->execute($sql);
    }

	public function down()
	{
		echo "m131126_140422_monitoring_notnull does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}