-- MySQL dump 10.15  Distrib 10.0.11-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: astraconf
-- ------------------------------------------------------
-- Server version	10.0.11-MariaDB-1~trusty

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `YiiSession`
--

DROP TABLE IF EXISTS `YiiSession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `YiiSession` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adapter`
--

DROP TABLE IF EXISTS `adapter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adapter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serverId` int(10) unsigned NOT NULL,
  `num` int(10) unsigned DEFAULT NULL,
  `device` int(10) unsigned DEFAULT NULL,
  `mac` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_adapter_server1_idx` (`serverId`),
  CONSTRAINT `fk_adapter_server1` FOREIGN KEY (`serverId`) REFERENCES `server` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `num` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `genre` int(10) unsigned NOT NULL,
  `sortPriority` int(10) unsigned NOT NULL DEFAULT '1000',
  `haveContract` int(1) unsigned NOT NULL DEFAULT '0',
  `isMandatory` int(1) NOT NULL DEFAULT '0',
  `isLicensed` int(1) NOT NULL DEFAULT '0',
  `subscriptionLevel` int(10) unsigned NOT NULL DEFAULT '0',
  `ssIptvId` int(10) unsigned DEFAULT NULL,
  `xmlId` varchar(45) DEFAULT NULL,
  `serverId` int(10) unsigned NOT NULL,
  `mosaicImageFileName` varchar(255) NOT NULL DEFAULT '',
  `lastMosaicImageUpdate` int(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_channel_server1_idx` (`serverId`),
  CONSTRAINT `fk_channel_server1` FOREIGN KEY (`serverId`) REFERENCES `server` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dvbCOptions`
--

DROP TABLE IF EXISTS `dvbCOptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dvbCOptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dvbTranspoderId` int(10) unsigned NOT NULL,
  `frequency` int(10) unsigned NOT NULL,
  `symbolrate` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dvbCOptions_dvbTranspoder2_idx` (`dvbTranspoderId`),
  CONSTRAINT `fk_dvbCOptions_dvbTranspoder2` FOREIGN KEY (`dvbTranspoderId`) REFERENCES `dvbTranspoder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dvbSOptions`
--

DROP TABLE IF EXISTS `dvbSOptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dvbSOptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dvbTranspoderId` int(10) unsigned NOT NULL,
  `frequency` int(10) unsigned NOT NULL,
  `polarization` varchar(4) NOT NULL,
  `symbolrate` int(10) unsigned NOT NULL,
  `lof1` int(10) unsigned NOT NULL,
  `lof2` int(10) unsigned NOT NULL,
  `slof` int(10) unsigned NOT NULL,
  `lnb_sharing` int(1) unsigned DEFAULT '0',
  `diseqc` int(1) unsigned DEFAULT '0',
  `tone` int(1) unsigned DEFAULT '0',
  `rolloff` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dvbSOptions_dvbTranspoder1_idx` (`dvbTranspoderId`),
  CONSTRAINT `fk_dvbSOptions_dvbTranspoder1` FOREIGN KEY (`dvbTranspoderId`) REFERENCES `dvbTranspoder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dvbTOptions`
--

DROP TABLE IF EXISTS `dvbTOptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dvbTOptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dvbTranspoderId` int(10) unsigned NOT NULL,
  `frequency` int(10) unsigned NOT NULL,
  `bandwidth` varchar(45) DEFAULT NULL,
  `guardinterval` varchar(45) DEFAULT NULL,
  `transmitmode` varchar(45) DEFAULT NULL,
  `hierarchy` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dvbTOptions_dvbTranspoder1_idx` (`dvbTranspoderId`),
  CONSTRAINT `fk_dvbTOptions_dvbTranspoder1` FOREIGN KEY (`dvbTranspoderId`) REFERENCES `dvbTranspoder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dvbTranspoder`
--

DROP TABLE IF EXISTS `dvbTranspoder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dvbTranspoder` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(45) DEFAULT NULL,
  `satellite` varchar(45) DEFAULT NULL,
  `adapterId` int(10) unsigned NOT NULL,
  `type` varchar(5) NOT NULL DEFAULT 'S',
  `budget` int(1) unsigned DEFAULT '0',
  `modulation` varchar(45) DEFAULT NULL,
  `fec` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dvbTranspoder_adapter1_idx` (`adapterId`),
  CONSTRAINT `fk_dvbTranspoder_adapter1` FOREIGN KEY (`adapterId`) REFERENCES `adapter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `monitoringEvent`
--

DROP TABLE IF EXISTS `monitoringEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monitoringEvent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channelId` int(10) unsigned NOT NULL,
  `streamId` int(10) unsigned DEFAULT NULL,
  `softcamServerId` int(10) unsigned DEFAULT NULL,
  `timestamp` int(20) unsigned NOT NULL,
  `pes_error` int(20) unsigned DEFAULT NULL,
  `cc_error` int(20) unsigned DEFAULT NULL,
  `bitrate` int(20) unsigned DEFAULT NULL,
  `scrambled` int(1) unsigned NOT NULL,
  `onair` int(1) unsigned NOT NULL,
  `fallback` int(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_monitoringData_channel1_idx` (`channelId`),
  KEY `fk_monitoringEvent_stream1_idx` (`streamId`),
  KEY `fk_monitoringEvent_softcamServer1_idx` (`softcamServerId`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `server`
--

DROP TABLE IF EXISTS `server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `server` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `enableHttpOutput` int(1) unsigned NOT NULL DEFAULT '1',
  `enableUdpOutput` int(1) unsigned NOT NULL DEFAULT '0',
  `keepHttpActive` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `softcamServer`
--

DROP TABLE IF EXISTS `softcamServer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `softcamServer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `host` varchar(45) DEFAULT NULL,
  `port` int(10) unsigned DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `aesKey` varchar(45) DEFAULT NULL,
  `sendEMM` int(1) unsigned NOT NULL DEFAULT '0',
  `separateConnections` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stream`
--

DROP TABLE IF EXISTS `stream`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stream` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `serverId` int(10) unsigned NOT NULL,
  `channelId` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dvbTranspoderId` int(10) unsigned DEFAULT NULL,
  `isEncoded` int(1) unsigned NOT NULL,
  `biss` varchar(45) DEFAULT NULL,
  `priority` int(10) unsigned NOT NULL DEFAULT '1000',
  `url` varchar(255) DEFAULT NULL,
  `pnr` int(10) unsigned DEFAULT NULL,
  `casData` varchar(45) DEFAULT NULL,
  `useBackup` int(1) unsigned NOT NULL DEFAULT '0',
  `filter` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stream_server_idx` (`serverId`),
  KEY `fk_stream_channel1_idx` (`channelId`),
  KEY `fk_stream_dvbTranspoder1_idx` (`dvbTranspoderId`),
  CONSTRAINT `fk_stream_channel1` FOREIGN KEY (`channelId`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stream_dvbTranspoder1` FOREIGN KEY (`dvbTranspoderId`) REFERENCES `dvbTranspoder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stream_server` FOREIGN KEY (`serverId`) REFERENCES `server` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stream_has_softcamServer`
--

DROP TABLE IF EXISTS `stream_has_softcamServer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stream_has_softcamServer` (
  `stream_id` int(10) unsigned NOT NULL,
  `softcamServer_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`stream_id`,`softcamServer_id`),
  KEY `fk_stream_has_softcamServer_softcamServer1_idx` (`softcamServer_id`),
  KEY `fk_stream_has_softcamServer_stream1_idx` (`stream_id`),
  CONSTRAINT `fk_stream_has_softcamServer_softcamServer1` FOREIGN KEY (`softcamServer_id`) REFERENCES `softcamServer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stream_has_softcamServer_stream1` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-19 11:23:30
-- MySQL dump 10.15  Distrib 10.0.11-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: astraconf
-- ------------------------------------------------------
-- Server version	10.0.11-MariaDB-1~trusty

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
INSERT INTO `tbl_migration` VALUES ('m131030_085215_license',1383123198),('m131118_144720_session',1384786071),('m131125_120616_mosaicDatabase',1385381238),('m131125_121202_mosaicDatabase_removeOld',1385381600),('m131126_122007_eventDatabase',1385469063),('m131126_125311_monitoring_autoIncrement',1385470680),('m131126_135412_monitoring_fallback',1385474175),('m131126_140422_monitoring_notnull',1385474711),('m131126_140836_monitoring_memory',1385475149),('m131127_120930_drop_fk',1385554201),('m131127_121057_recreate_fk',1385554323),('m131202_095401_softCam_multiple',1385978071),('m140324_120245_stream_filter',1395662605),('m140512_075706_outputMethods',1399881571),('m140512_131932_keepHttpActive',1399900831),('m140519_081755_dvb_c',1400487558);
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-19 11:23:30
