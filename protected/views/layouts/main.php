<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

    <title><?php echo CHtml::encode($this->pageHeader); ?></title>
</head>

<body>

<div class="container" id="page">

    <div id="header">
        <div class="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
        <div class="logo fullscreenSwitch"><?php
            echo CHtml::link(
                CHtml::image(Yii::app()->assetManager->publish(Yii::app()->basePath.'/data/fullscreen.png')),
                $this->createUrl($this->route, CMap::mergeArray($_GET, array('fullscreen' => 1)))
            );
            ?></div>
    </div><!-- header -->
    <div id="mainmenu">
        <?php
        $this->renderPartial('//layouts/_menu');
        ?>
    </div><!-- mainmenu -->
    <?php if(isset($this->breadcrumbs)):?>
        <?php
        if(isset($this->breadcrumbs))
        {
            if ($this->pageHeader !== null)
                $this->breadcrumbs[] = $this->pageHeader;
            $this->widget('zii.widgets.CBreadcrumbs', array(
                'links'=>$this->breadcrumbs,
            ));
        }
        ?>
    <?php endif?>

    <?php echo $content; ?>

    <div class="clear"></div>

    <div id="footer">
        Copyright &copy; <?php echo date('Y'); ?> by <a href="http://cesbo.com/forum/user/185-abram/">Vadym Abramchuk</a> .<br/>
        All Rights Reserved.<br/>
        <?php echo Yii::powered(); ?>
    </div><!-- footer -->

</div><!-- page -->

</body>
</html>
