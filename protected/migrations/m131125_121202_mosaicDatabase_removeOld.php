<?php

class m131125_121202_mosaicDatabase_removeOld extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
ALTER TABLE `channel`
DROP COLUMN `newMosaicImageFileName` ,
DROP COLUMN `oldMosaicImageFileName` ,
ADD COLUMN `mosaicImageFileName` VARCHAR(255) NOT NULL DEFAULT ''  AFTER `serverId`;
SQL;
        $this->execute($sql);
	}

	public function down()
	{
		echo "m131125_121202_mosaicDatabase_removeOld does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}