<?php

class m131126_135412_monitoring_fallback extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
ALTER TABLE `monitoringEvent`
CHANGE COLUMN `streamId` `streamId` INT(10) UNSIGNED NULL DEFAULT NULL,
ADD COLUMN `fallback` INT(1) UNSIGNED NOT NULL  AFTER `onair`
SQL;
        $this->execute($sql);
	}

	public function down()
	{
		echo "m131126_135412_monitoring_fallback does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}