<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageHeader = Yii::t('app', 'Ошибка {CODE}', array('{CODE}' => $code));
?>

<div class="error">
<?php echo CHtml::encode($message); ?>
</div>