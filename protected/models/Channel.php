<?php

/**
 * This is the model class for table "channel".
 *
 * The followings are the available columns in table 'channel':
 * @property string $id
 * @property string $num
 * @property string $name
 * @property string $genre
 * @property string $sortPriority
 * @property integer $haveContract
 * @property integer $isMandatory
 * @property integer $isLicensed
 * @property string $subscriptionLevel
 * @property string $ssIptvId
 * @property string $xmlId
 * @property string $epgCorrection
 * @property string $serverId
 * @property string $mosaicImageFileName
 * @property string $lastMosaicImageUpdate
 *
 * The followings are the available model relations:
 * @property Server $server
 * @property Stream[] $streams
 * @property MonitoringEvent[] $monitoringEvents
 *
 * @property string $httpUrl
 * @property string $outputHttpUrl
 * @property string $outputUdpUrl
 * @property string $analyzeUrl
 * @property string $clientUrl
 * @property string $canCast
 * @property bool $directCast
 * @property string $serverLink
 * @property string $haveContractString
 * @property string $isMandatoryString
 * @property string $isLicensedString
 * @property MonitoringEvent $lastMonitoringEvent
 *
 * @property string $mosaicImageUrl
 */
class Channel extends MyActiveRecord
{

    private static $clickableCssRegistered = false;
    private  $_canCast = null;
    public $canCastSearch;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'channel';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('num, name, genre, serverId', 'required'),
            array('haveContract, isMandatory, isLicensed, lastMosaicImageUpdate, epgCorrection', 'numerical', 'integerOnly'=>true),
            array('num, genre, sortPriority, subscriptionLevel, epgCorrection, ssIptvId, serverId', 'length', 'max'=>10),
            array('name, mosaicImageFileName', 'length', 'max'=>255),
            array('haveContract, isMandatory, isLicensed', 'length', 'max'=>1),
            array('xmlId', 'length', 'max'=>45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, num, name, genre, sortPriority, haveContract, isMandatory, isLicensed, subscriptionLevel, ssIptvId, xmlId, canCastSearch, mosaicImageFileName, lastMosaicImageUpdate', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'server' => array(self::BELONGS_TO, 'Server', 'serverId'),
            'streams' => array(self::HAS_MANY, 'Stream', 'channelId', 'order' => '`streams`.`priority` DESC, `streams`.`id`'),
            'monitoringEvents' => array(self::HAS_MANY, 'MonitoringEvent', 'channelId', 'order' => '`monitoringEvents`.`timestamp` ASC'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'num' => Yii::t('models', 'Номер'),
            'name' => Yii::t('models', 'Имя'),
            'genre' => Yii::t('models', 'Жанр'),
            'genreLabel' => Yii::t('models', 'Жанр'),
            'sortPriority' => Yii::t('models', 'Приоритет при сортировке'),
            'haveContract' => Yii::t('models', 'Есть договор'),
            'isMandatory' => Yii::t('models', 'Обязателен'),
            'isLicensed' => Yii::t('models', 'Включен в лицензию'),
            'haveContractString' => Yii::t('models', 'Есть договор'),
            'isMandatoryString' => Yii::t('models', 'Обязателен'),
            'isLicensedString' => Yii::t('models', 'Включен в лицензию'),
            'subscriptionLevel' => Yii::t('models', 'Тариф'),
            'ssIptvId' => Yii::t('models', 'ID в SS IPTV (LG Smart TV)'),
            'xmlId' => Yii::t('models', 'ID в XMLTV'),
            'epgCorrection' => Yii::t('models', 'Коррекция EPG (в минутах)'),
            'serverId' => Yii::t('models', 'Сервер'),
            'serverLink' => Yii::t('models', 'Сервер вещания'),
            'canCast' => Yii::t('models', 'Проводится вещание'),
            'canCastString' => Yii::t('models', 'Проводится вещание'),
            'canCastSearch' => Yii::t('models', 'Проводится вещание'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;
        $criteria->alias = 'channel';

        $criteria->join .=
            'LEFT OUTER JOIN (
            SELECT `has`.`ss_count`, `channelId`, COUNT(id) as `sc` FROM `stream` LEFT OUTER JOIN (
            SELECT `stream_id`, COUNT(`softcamServer_id`) as `ss_count` FROM `stream_has_softcamServer` GROUP BY `stream_id`
            ) `has` ON `has`.`stream_id` = `stream`.`id`
            WHERE `isEncoded` = '.intval(Stream::ENC_OPEN).' OR (`isEncoded` = '.intval(Stream::ENC_BISS).' AND LENGTH(`biss`) > 2) OR (`isEncoded` = '.intval(Stream::ENC_CI).') OR (`isEncoded` = '.intval(Stream::ENC_CAM).' AND `has`.`ss_count` > 0) GROUP BY `channelId`
            )
            `streamCount` ON `channel`.`id` = `streamCount`.`channelId`';

        $sort = new CSort();
        $sort->defaultOrder = '`channel`.`sortPriority` ASC, `channel`.`genre` ASC, `channel`.`name` ASC, `channel`.`subscriptionLevel` ASC';
        $sort->attributes = array(
            '*',
            'canCastSearch' => array(
                'asc' => '`streamCount`.`sc` ASC',
                'desc' => '`streamCount`.`sc` DESC',
            )
        );

        $criteria->compare('id',$this->id);
        $criteria->compare('num',$this->num);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('genre',$this->genre);
        $criteria->compare('sortPriority',$this->sortPriority);
        $criteria->compare('haveContract',$this->haveContract);
        $criteria->compare('isMandatory',$this->isMandatory);
        $criteria->compare('isLicensed',$this->isLicensed);
        $criteria->compare('subscriptionLevel',$this->subscriptionLevel);
        $criteria->compare('ssIptvId',$this->ssIptvId);
        $criteria->compare('xmlId',$this->xmlId,true);
        $criteria->compare('epgCorrection',$this->xmlId);
        $criteria->compare('serverId',$this->serverId);

        if (is_numeric($this->canCastSearch))
        {
            if ($this->canCastSearch)
            {
                $criteria->compare('`streamCount`.`sc`','>0');
                if (Yii::app()->params['strictMode'])
                {
                    $manCriteria = new CDbCriteria();
                    $manCriteria->compare('`channel`.`isMandatory`', true);
                    $licenseAndContractCriteria = new CDbCriteria();
                    $licenseAndContractCriteria->compare('`channel`.`isLicensed`', true);
                    $licenseAndContractCriteria->compare('`channel`.`haveContract`', true);
                    $strictCriteria = new CDbCriteria();
                    $strictCriteria->mergeWith($manCriteria);
                    $strictCriteria->mergeWith($licenseAndContractCriteria, 'OR');
                    $criteria->mergeWith($strictCriteria, 'AND');
                }
                else
                    $criteria->compare('`channel`.`subscriptionLevel`', '<='.intval(Yii::app()->params['maximalSubscriptionLevel']));
            }
            else
                $criteria->addCondition('`streamCount`.`sc` IS NULL OR `streamCount`.`sc` = 0');
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize' => 40,
            ),
            'sort'=>$sort,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Channel the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function getGenresList()
    {
        return Yii::app()->params['tvGenres'];
    }

    public static function genreLabel($id)
    {
        if (!$id)
            return $id;
        $genres = self::getGenresList();
        if (isset($genres[$id]))
            return $genres[$id];
        else
            return null;
    }

    public function getGenreLabel()
    {
        return self::genreLabel($this->genre);
    }

    public static function genUdpUrl($id, $interface = null)
    {
        return 'udp://'.($interface ? $interface.'@' : '').long2ip(ip2long('239.255.1.0') + $id).':1234';
    }

    public function getHttpUrl($externalIp = null)
    {
        if (is_null($externalIp))
            $externalIp = $this->server->ip;
        $url = new AstraUrl('http', $externalIp, 8000+$this->num, $this->num);
        return $url->asString();
    }

    public function getOutputHttpUrl()
    {
        if (isset(Yii::app()->params['http_options']))
            $params = Yii::app()->params['http_options'];
        else
            $params = array(
                'buffer_fill=1024',
                'buffer_size=10240',
            );
        if ($this->server->keepHttpActive)
            $params[] = 'keep_active';
        $url = new AstraUrl('http', $this->server->ip, 8000+$this->num, $this->num, $params);
        return $url->asString();
    }

    public function getOutputUdpUrl()
    {
        $url = new AstraUrl('udp', long2ip(ip2long('239.255.1.0') + $this->id), 1234);
        return $url->asString();
    }

    public function getAnalyzeUrl()
    {
        if ($this->server->enableHttpOutput)
            return $this->getHttpUrl();
        if ($this->server->enableUdpOutput)
        {
            if (Yii::app()->params['udpAnalyzeInterface'])
                return AstraUrl::fromString($this->outputUdpUrl)->setUdpInterface(Yii::app()->params['udpAnalyzeInterface'])->asString();
            else
                return $this->outputUdpUrl;
        }
        return null;
    }

    public function getClientUrl($externalIp = false)
    {
        if ($this->server->enableHttpOutput)
            if ($externalIp)
                return $this->getHttpUrl($this->server->externalIp);
            else
                return $this->getHttpUrl();
        if ($this->server->enableUdpOutput)
            return $this->outputUdpUrl;
        return null;
    }

    public function getCanCast()
    {
        if (is_null($this->_canCast))
        {
            //TODO: check for license, contract etc
            $castingCount = 0;
            foreach ($this->streams as $stream)
            {
                if ($stream->canCast)
                    $castingCount++;
            }
            $this->_canCast = ($castingCount > 0);
            $strictMode = Yii::app()->params['strictMode'];
            if ($this->_canCast && $strictMode)
            {
                if (!$this->isMandatory)
                {
                    if (!$this->isLicensed || !$this->haveContract)
                        $this->_canCast = false;
                }
            }
        }
        return $this->_canCast;
    }

    public function setCanCast($val)
    {
        $this->_canCast = $val;
    }

    public function getDirectCast()
    {
        $streams = $this->streams;
        $canCastStreams = array();
        foreach($streams as $stream)
        {
            if ($stream->canCast)
                $canCastStreams[] = $stream;
        }
        $canCastStreamsCount = count($canCastStreams);
        if ($canCastStreamsCount == 0)
            return true;
        if ($canCastStreamsCount == 1)
        {
            $firstStream = array_shift($canCastStreams);
            /* @var $firstStream Stream */
            if ($firstStream->serverId != $this->serverId)
                return false;
            else
                return true;
        }
        if ($canCastStreamsCount > 1)
            return false;

        // whatever
        return true;
    }

    public function getServerLink()
    {
        return CHtml::link($this->server->name, array('/servers/view', 'id' => $this->serverId));
    }

    public function getHaveContractString()
    {
        return self::boolStringHtml($this->haveContract);
    }

    public function getIsMandatoryString()
    {
        return self::boolStringHtml($this->isMandatory);
    }

    public function getIsLicensedString()
    {
        return self::boolStringHtml($this->isLicensed);
    }

    public function getCanCastString()
    {
        return self::boolStringHtml($this->canCast);
    }

    public function getHaveContractClickableString()
    {
        self::registerClickableCss();
        $spanId = 'haveContractClickable_'.$this->id;
        return CHtml::ajaxLink(self::boolStringHtml($this->haveContract, $spanId), array('/channels/switchHaveContract', 'id' => $this->id), array('update' => '#'.$spanId), array('class' => 'clickableLink'));
    }

    public function getIsMandatoryClickableString()
    {
        self::registerClickableCss();
        $spanId = 'isMandatoryClickable_'.$this->id;
        return CHtml::ajaxLink(self::boolStringHtml($this->isMandatory, $spanId), array('/channels/switchIsMandatory', 'id' => $this->id), array('update' => '#'.$spanId), array('class' => 'clickableLink'));
    }

    public function getIsLicensedClickableString()
    {
        self::registerClickableCss();
        $spanId = 'isLicensedClickable_'.$this->id;
        return CHtml::ajaxLink(self::boolStringHtml($this->isLicensed, $spanId), array('/channels/switchIsLicensed', 'id' => $this->id), array('update' => '#'.$spanId), array('class' => 'clickableLink'));
    }

    public static function registerClickableCss()
    {
        if (!self::$clickableCssRegistered)
        {
            Yii::app()->clientScript->registerCss('clickableChannelLink',
                '.clickableLink {text-decoration: none}'
            );
            self::$clickableCssRegistered = true;
        }
    }

    public function getMosaicImageUrl($mayRefresh = true, $forceRefresh = false)
    {
        if ($mayRefresh && ($forceRefresh || $this->lastMosaicImageUpdate == 0 || $this->lastMosaicImageUpdate + Yii::app()->params['mosaicRefreshInterval'] <= time()))
            $this->refreshMosaicImage();
        if (strlen(trim($this->mosaicImageFileName)) == 0 && (Yii::app()->getComponent('assetManager')))
            return Yii::app()->assetManager->publish(Yii::app()->basePath.'/data/error.png');
        return Yii::app()->baseUrl.'/mosaic/'.$this->mosaicImageFileName;
    }

    public function refreshMosaicImage()
    {
        $time = time();
        $filename = 'mosaic_'.$this->id.'_'.$time.'.'.Yii::app()->params['mosaicImageFormat'];
        $realFilename = Yii::app()->basePath.'/../webroot/mosaic/'.$filename;
        if ($this->canCast)
        {
            try
            {
                switch (Yii::app()->params['mosaicImageFormat'])
                {
                    case 'gif':
                        $cmd = "avconv -i ".escapeshellarg($this->analyzeUrl)." -ss 00:00:04.000 -y -r ".escapeshellarg(intval(Yii::app()->params['mosaicGifFrames']))." -t 1 -vf scale=320:240,format=rgb8,format=rgb24 -pix_fmt rgb24 -f gif ".escapeshellarg($realFilename)." 2>&1";
                        break;
                    default:
                        $cmd = "avconv -i ".escapeshellarg($this->analyzeUrl)." -ss 00:00:04.000 -y -vframes 1 -t 1 -f image2 ".escapeshellarg($realFilename)." 2>&1";
                        break;
                }
                $out = self::ExecWaitTimeout($cmd, 30);
                if (!(file_exists($realFilename) && filesize($realFilename) > 0))
                    $filename = '';
                $oldFileName = $this->mosaicImageFileName;
                $this->lastMosaicImageUpdate = $time;
                $this->mosaicImageFileName = $filename;
                if ($this->save() && strlen($oldFileName) > 0)
                {
                    $oldRealFilename = Yii::app()->basePath.'/../webroot/mosaic/'.$oldFileName;
                    if (file_exists($oldRealFilename))
                        unlink($oldRealFilename);
                }
            }
            catch (Exception $e) {
                Yii::log('Mosaic creation exception: '.$e->getMessage().', cmd was :'.PHP_EOL.$cmd, CLogger::LEVEL_ERROR);
                if (file_exists($realFilename))
                    unlink($realFilename);
                $this->lastMosaicImageUpdate = $time;
                $this->mosaicImageFileName = '';
                $this->save();
            }
        }
        else
        {
            throw new CException("Can't cast channel ".$this->name.", so mosaic is not possible.");
        }
    }

    /**
     * Execute a command and kill it if the timeout limit fired to prevent long php execution
     *
     * @see http://stackoverflow.com/questions/2603912/php-set-timeout-for-script-with-system-call-set-time-limit-not-working
     *
     * @param string $cmd Command to exec (you should use 2>&1 at the end to pipe all output)
     * @param integer $timeout
     * @return string Returns command output
     */
    public static function ExecWaitTimeout($cmd, $timeout=5) {
        $descriptorspec = array(
            0 => array("pipe", "r"),
            1 => array("pipe", "w"),
            2 => array("pipe", "w")
        );
        $pipes = array();

        $timeout += time();
        $process = proc_open($cmd, $descriptorspec, $pipes);
        if (!is_resource($process)) {
            throw new Exception("proc_open failed on: " . $cmd);
        }

        $output = '';

        do {
            $timeleft = $timeout - time();
            $read = array($pipes[1]);
            $write = array();
            $ex = array();
            stream_select($read, $write, $ex, $timeleft, NULL);

            if (!empty($read)) {
                $output .= fread($pipes[1], 8192);
            }
        } while (!feof($pipes[1]) && $timeleft > 0);

        if ($timeleft <= 0) {
            proc_terminate($process);
            throw new Exception("command timeout on: " . $cmd);
        } else {
            return $output;
        }
    }

    public function delete()
    {
        $retval = false;
        $useTransaction = !$this->dbConnection->currentTransaction;
        if ($useTransaction)
            $transaction = $this->dbConnection->beginTransaction();
        try
        {
            foreach ($this->streams as $stream)
            {
                if (!$stream->delete())
                    throw new CException('Error deleting stream '.$stream->id);
            }
            $retval = parent::delete();
            if ($useTransaction)
                $transaction->commit();
        }
        catch(Exception $e)
        {
            Yii::log('Error deleting channel '.$this->id.', caught exception: '.$e->getMessage(), CLogger::LEVEL_ERROR);
            if ($useTransaction)
                $transaction->rollback();
        }
        return $retval;
    }

    public function getLastMonitoringEvent()
    {
        $criteria = new CDbCriteria();
        $criteria->limit = 1;
        $criteria->alias = 'event';
        $criteria->order = '`event`.`timestamp` DESC';
        return MonitoringEvent::model()->findByAttributes(array('channelId' => $this->id), $criteria);
    }

}
