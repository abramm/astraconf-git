<?php

class ImportCommand extends CConsoleCommand {

    public function actionImportFromScan($serverIp, $filename, $adapterNum)
    {
        $transaction=Yii::app()->db->beginTransaction();
        try
        {
            $server = Server::model()->findByAttributes(array('ip' => $serverIp));
            if (!$server)
            {
                $server = new Server();
                $server->ip = $serverIp;
                $server->name = 'Imported '.$serverIp;
                if (!$server->save())
                    throw new CException('Error saving server');
            }

            $oldConfigLines = file($filename);
            if (!is_array($oldConfigLines))
                throw new CException('Error opening '.$filename);

            $channelsData = self::readScanChannelLists($oldConfigLines);
            if (count($channelsData) < 1)
                throw new CException('Empty channels data from '.$filename);

            $adapter = Adapter::model()->findByAttributes(array('serverId' => $server->id, 'num' => $adapterNum));
            if (!$adapter)
            {
                $adapter = new Adapter();
                $adapter->num = $adapterNum;
                $adapter->serverId = $server->id;
                if (!$adapter->save())
                    throw new CException('Error saving adapter');
            }

            $dvbSSettings['frequency'] = $channelsData[0]['transpoder'];
            $dvbSSettings['polarization'] = $channelsData[0]['polarization'];
            $dvbSSettings['symbolrate'] = $channelsData[0]['speed'];
            if (strtolower($channelsData[0]['polarization']) == 'l' || strtolower($channelsData[0]['polarization']) == 'r')
            {
                $dvbSSettings['lof1'] = 10750;
                $dvbSSettings['lof2'] = 10750;
                $dvbSSettings['slof'] = 10750;
            }
            else
            {
                $dvbSSettings['lof1'] = 9750;
                $dvbSSettings['lof2'] = 10600;
                $dvbSSettings['slof'] = 11700;
            }

            $transpoderOptions = DvbSOptions::model()->findByAttributes($dvbSSettings);
            if (!$transpoderOptions)
            {
                $transpoder = new DvbTranspoder();
                $transpoder->adapterId = $adapter->id;
                $transpoder->type = 'S';
                $transpoder->description = $dvbSSettings['frequency'].$dvbSSettings['polarization'].' at '.$server->ip.' adapter '.$adapterNum;

                if (!$transpoder->save())
                    throw new CException('Error saving transpoder: '.var_export($transpoder->errors, true));

                $transpoderOptions = new DvbSOptions();
                $transpoderOptions->attributes = $dvbSSettings;
                $transpoderOptions->dvbTranspoderId = $transpoder->id;
                if (!$transpoderOptions->save())
                    throw new CException('Error saving DVB-S transpoder settings: '.var_export($transpoderOptions->errors, true));

            }

            $transpoder = $transpoderOptions->dvbTranspoder;

            $channelNum = Channel::model()->dbConnection->createCommand('SELECT MAX(num) FROM `channel`;')->queryScalar() + 1;

            foreach($channelsData as $thisChannel)
            {
                $channel = Channel::model()->findByAttributes(array('name' => $thisChannel['name']));
                if (!$channel)
                {
                    $channel = new Channel();
                    $channel->num = $channelNum++;
                    $channel->name = $thisChannel['name'];
                    $channel->genre = 2;
                    $channel->sortPriority = 1000;
                    $channel->haveContract = 0;
                    $channel->subscriptionLevel = 10;
                    $channel->ssIptvId = null;
                    $channel->xmlId = null;
                    $channel->serverId = $server->id;
                    if (!$channel->save())
                        throw new CException('Error saving channel '.$channel->name.': '.var_export($channel->errors, true));
                }

                $stream = Stream::model()->findByAttributes(array('serverId' => $server->id, 'channelId' => $channel->id, 'type' => Stream::TYPE_DVB));
                if (!$stream)
                {
                    $stream = new Stream();
                    $stream->serverId = $server->id;
                    $stream->dvbTranspoderId = $transpoder->id;
                    $stream->channelId = $channel->id;
                    $stream->type = Stream::TYPE_DVB;
                    $stream->name = $channel->name.' from '.$transpoderOptions->frequency.$transpoderOptions->polarization;
                    $stream->description = 'Imported stream';
                    $stream->pnr = $thisChannel['pnr'];
                    $stream->useBackup = 1;

                    $keyServer = null;

                    $stream->isEncoded = Stream::ENC_UNKNOWN;

                    if (!$stream->save())
                        throw new CException('Error saving stream for '.$channel->name.': '.var_export($stream->errors, true));
                }
            }

            $transaction->commit();
        }
        catch(Exception $e)
        {
            $transaction->rollback();
            echo 'ERROR: ', $e->getMessage();
            throw $e;
        }

    }

    public static function readScanChannelLists($rawChannels)
    {
        $channels = array();
        foreach ($rawChannels as $rawChannel)
        {
            if (strlen(trim($rawChannel)))
            {
                $channel = explode(':',trim($rawChannel));
                if (count($channel) > 1)
                {
                    $channels[] = array(
                        'name' => $channel[0],
                        'transpoder' => $channel[1],
                        'polarization' => $channel[2],
                        'tmp0' => $channel[3],
                        'speed' => $channel[4],
                        'tmp1' => $channel[5],
                        'tmp2' => $channel[6],
                        'pnr' => $channel[7],
                    );
                }
            }
        }

        return $channels;
    }

    public function actionImportFromPlaylist($serverIp, $filename)
    {
        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            $server = Server::model()->findByAttributes(array('ip' => $serverIp));
            if (!$server)
            {
                $server = new Server();
                $server->ip = $serverIp;
                $server->name = 'Imported '.$serverIp;
                if (!$server->save())
                    throw new CException('Error saving server');
            }

            $playlistLines = file($filename);
            if (!is_array($playlistLines))
                throw new CException('Error opening '.$filename);

            $matches = array();
            $channelName = '';
            $channelNum = Channel::model()->dbConnection->createCommand('SELECT MAX(num) FROM `channel`;')->queryScalar() + 1;

            foreach ($playlistLines as $line)
            {
                $line = trim($line);
                if ($line == '#EXTM3U' || $line == '')
                {
                    $channelName = '';
                }
                elseif (preg_match('/\#EXTINF:-?\d*,(.*)/', $line, $matches))
                {
                    $channelName = $matches[1];
                }
                elseif (strpos($line, '://') !== false)
                {
                    $url = $line;
                    $channel = Channel::model()->findByAttributes(array('name' => $channelName));
                    if (!$channel)
                    {
                        $channel = new Channel();
                        $channel->num = $channelNum++;
                        $channel->name = $channelName;
                        $channel->serverId = $server->id;
                        $channel->genre = 0;
                        $channel->subscriptionLevel = 5;
                        if (!$channel->save())
                            throw new CException('Error saving channel "'.$channel->name.'" ("'.$url.'"): '.var_export($channel->errors, true));
                    }

                    $stream = Stream::model()->findByAttributes(array(
                        'channelId' => $channel->id,
                        'serverId' => $server->id
                    ));

                    if (!$stream)
                    {
                        $stream = new Stream();
                        $stream->name = $channelName.' URL';
                        $stream->serverId = $server->id;
                        $stream->channelId = $channel->id;
                        $stream->isEncoded = Stream::ENC_OPEN;
                        $stream->type = Stream::TYPE_URL;
                    }
                    $stream->url = $url;
                    if (!$stream->save())
                        throw new CException('Error saving stream: '.var_export($stream->errors, true));
                }

            }
            $transaction->commit();
        }
        catch(Exception $e)
        {
            $transaction->rollback();
            echo 'ERROR: ', $e->getMessage();
            throw $e;
        }
    }


}
