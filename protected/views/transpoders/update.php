<?php
/**
 * @var $this TranspodersController
 * @var $model DvbTranspoder
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список трансподеров') => array('/transpoders/index'),
);

$this->pageHeader = Yii::t('menu','Редактирование трансподера {NAME}', array('{NAME}' => $model->description));
?>
<p>
    <?php
    $this->renderPartial('_form', array('model' => $model));
    ?>
</p>
