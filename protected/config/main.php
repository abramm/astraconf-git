<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$conf = array(
    'sourceLanguage' => 'ru',
    'language' => 'ru',

    'modules'=>array(
//        'gii'=>array(
//            'class'=>'system.gii.GiiModule',
//            'password'=>'S3cr3t',
//            // If removed, Gii defaults to localhost only. Edit carefully to taste.
//            'ipFilters'=>array('127.0.0.1','::1'),
//        ),
    ),

    // application components
    'components'=>array(
        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
        ),

        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'routes'=>array(
                // add here
            ),
        ),
//        'debug' => array(
//            'class' => 'ext.yii2-debug.Yii2Debug',
//        ),
        'session' => array (
            'class' => 'system.web.CDbHttpSession',
            'connectionID' => 'db',
            'autoCreateSessionTable' => false,
            'gCProbability' => 5,
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']

);

$dbConf = require('db.php');
$commonConf = require('common.php');

$conf = CMap::mergeArray($conf, $dbConf);
$conf = CMap::mergeArray($conf, $commonConf);


return $conf;