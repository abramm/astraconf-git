<?php
/* @var $this DvbTranspoderController */
/* @var $model DvbTranspoder */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dvb-transpoder-_form-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note"><?= Yii::t('models', 'Поля с {*} обязательны к заполнению', array('{*}' => '<span class="required">*</span>')); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'adapterId'); ?>
		<?php echo $form->dropDownList($model,'adapterId', CHtml::listData(Adapter::model()->findAll(),'id', 'displayName')); ?>
		<?php echo $form->error($model,'adapterId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description'); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'satellite'); ?>
		<?php echo $form->textField($model,'satellite'); ?>
		<?php echo $form->error($model,'satellite'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modulation'); ?>
		<?php echo $form->textField($model,'modulation'); ?>
		<?php echo $form->error($model,'modulation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fec'); ?>
		<?php echo $form->textField($model,'fec'); ?>
		<?php echo $form->error($model,'fec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',DvbTranspoder::getPossibleDVBTypes()); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'budget'); ?>
		<?php echo $form->checkBox($model,'budget'); ?>
		<?php echo $form->error($model,'budget'); ?>
	</div>

    <?php

    switch($model->type)
    {
        case 'S':
        case 'S2':
            $options = $model->dvbSOptions;
            if (!$options)
            {
                $options = new DvbSOptions();
                $options->dvbTranspoderId = $model->id;
            }
            $this->renderPartial('_dvbS', array('model' => $options, 'form' => $form));
            break;
        case 'T':
        case 'T2':
            $options = $model->dvbTOptions;
            if (!$options)
            {
                $options = new DvbTOptions();
                $options->dvbTranspoderId = $model->id;
            }
            $this->renderPartial('_dvbT', array('model' => $options, 'form' => $form));
            break;
        case 'C':
            $options = $model->dvbCOptions;
            if (!$options)
            {
                $options = new DvbCOptions();
                $options->dvbTranspoderId = $model->id;
            }
            $this->renderPartial('_dvbC', array('model' => $options, 'form' => $form));
            break;
        default:
            break;
    }

    ?>

	<div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('app', 'Сохранить')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->