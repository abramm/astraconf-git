<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 */
return array (
  'SoftCAM, в том числе несколько SoftCAM на поток;' => 'SoftCAM, включително няколко SoftCAM на поток',
  'Автоподстановка "матраса" (видео-заглушки в момент недоступности канала);' => 'Автоустановка "матрака" (видео входа в момента е недостъпен)',
  'Автор - {LINK}. Изначально Astraconf писался для себя, но было принято решение выложить продукт в open source. Сейчас уже несколько человек внесли свой вклад.' => 'Автор - {LINK}. Отначало Astraconf я писах за себе си, но взех решение да пусна продукта Open Source.Засега още няколко човека дадоха своя принос.',
  'Генерация конфигурации для DVB-S/S2;' => 'Генератор на конфигурации за DVB-S/S2',
  'Генерация конфигурации для DVB-T/T2;' => 'Генератор на конфигурации за DVB-T/T2',
  'Для начала работы выберите пункт из меню сверху.' => 'За първоначална работа изберете точка от менюто по-горе',
  'Импорт потоков/каналов из вывода команды scan;' => 'Внасяне поток/канал от изхода на командата scan',
  'Кто это делает? Зачем?' => 'Кой го прави? Защо?',
  'Купите платную версию Astra-ы. Astra кормит всех нас, поэтому в первую очередь поддержите её автора.' => 'Купете платената версия на Astra. Astra храни всички ни, затова подкрепете нейния автор',
  'Мне понравилось, я хочу помочь! Что делать?' => 'Хареса ми, искам да помогна! Какво да правя?',
  'Мозаика из каналов (нужно тестирование).' => 'Мозайка на каналите (нуждае се от тестове)',
  'Нашли ошибку? {LINK}. Детально опишите, как воспроизвести ошибку, и её обязательно исправят.' => 'Намерихте грешка? {LINK}. Опишете с детайли как сте достигнали до тази грешка и ние ще я оправим.',
  'Отлично! Способов помочь несколько:' => 'Отлично! Ето няколко начина, с които да помогнете:',
  'Переведите Astraconf на свой родной язык. При необходимости я обьясню, как это сделать.' => 'Преведете Astraconf на своя роден език. При необходимост ще обясним как да го направите.',
  'Пишите код, улучшайте, добавляйте возможности - я с радостью приму любую помощь.' => 'Пишете код, обновявайте, добавяйте възможност - с радост приемаме всякаква помощ',
  'Поддерживаются следующие возможности:' => 'Поддържат се следните възможности:',
  'Подзравляем! Astraconf установлен и готов к работе.' => 'Поздравления! Astraconf е инсталиран и готов за работа.',
  'Ретрансляция потоков, полученых по HTTP либо udp multicast;' => 'Извод на поток получен по HTTP или udp MultiCast.',
  'Сообщите мне' => 'Свържете се с мен',
  'Управление неограниченным количеством серверов;' => 'Управление на неограничено количество сървъри.',
  'Формирование плейлиста;' => 'Формиране на плейлиста',
  'Что такое Astraconf?' => 'Какво е Astraconf',
  'Это система управления серверами вещания цифрового телевидения на основе astra.' => 'Това е система за управление на сървъри излъчващи IPTV на основа "Astra"',
  'ваш покорный слуга' => 'ваш верен слуга',
);
