<?php
/* @var $this AdapterController */
/* @var $model Adapter */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'adapter-_form-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note"><?= Yii::t('models', 'Поля с {*} обязательны к заполнению', array('{*}' => '<span class="required">*</span>')); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'serverId'); ?>
		<?php echo $form->dropDownList($model,'serverId',CHtml::listData(Server::model()->findAll(),'id', 'name')); ?>
		<?php echo $form->error($model,'serverId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'num'); ?>
		<?php echo $form->textField($model,'num'); ?>
		<?php echo $form->error($model,'num'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'device'); ?>
		<?php echo $form->textField($model,'device'); ?>
		<?php echo $form->error($model,'device'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mac'); ?>
		<?php echo $form->textField($model,'mac'); ?>
		<?php echo $form->error($model,'mac'); ?>
	</div>


	<div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('app', 'Сохранить')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->