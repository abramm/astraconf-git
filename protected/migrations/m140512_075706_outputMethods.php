<?php

class m140512_075706_outputMethods extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
ALTER TABLE `server` ADD COLUMN `enableHttpOutput` INT(1) UNSIGNED NOT NULL DEFAULT 1  AFTER `ip` , ADD COLUMN `enableUdpOutput` INT(1) UNSIGNED NOT NULL DEFAULT 0  AFTER `enableHttpOutput` ;
SQL;
        $this->execute($sql);
	}

	public function down()
	{
		echo "m140512_075706_outputMethods does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}