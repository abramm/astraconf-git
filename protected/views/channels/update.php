<?php
/**
 * @var $this ChannelsController
 * @var $model Channel
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список каналов') => array('/channels/index'),
);

$this->pageHeader = Yii::t('menu','Редактирование канала {NAME}', array('{NAME}' => $model->name));
?>
<p>
    <?php
    $this->renderPartial('_form', array('model' => $model));
    ?>
</p>
