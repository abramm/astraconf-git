<?php
/**
 * @var $this SoftcamServersController
 * @var $model SoftcamServer
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список серверов ключей') => array('/softcamServers/index'),
);

$this->pageHeader = Yii::t('menu','Редактирование сервера ключей {NAME}', array('{NAME}' => $model->name));
?>
<p>
    <?php
    $this->renderPartial('_form', array('model' => $model));
    ?>
</p>
