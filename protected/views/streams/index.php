<?php
/**
 * @var $this StreamsController
 * @var $model Stream
 */

$this->pageHeader = Yii::t('menu','Список потоков');
?>
<p>
    <?php
    $dataProvider=$model->search();

    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'columns' => array(
            'name',
            'description',
            array(
                'name' =>'serverId',
                'value' => '$data->serverLink',
                'type' => 'html',
                'filter' => CHtml::listData(Server::model()->findAll(), 'id', 'name'),
            ),
            array(
                'name' => 'channelId',
                'value' => '$data->channelLink',
                'type' => 'html',
                'filter' => false,
            ),
            array(
                'name' => 'type',
                'value' => '$data->typeLabel',
                'filter' => Stream::getTypesList(),
            ),
            array(
                'name' => 'dvbTranspoderId',
                'value' => '$data->dvbTranspoderLink',
                'type' => 'html',
                'filter' => CHtml::listData(DvbTranspoder::model()->findAll(), 'id', 'description'),
            ),
            array(
                'name' => 'isEncoded',
                'value' => '$data->isEncodedLabel',
                'filter' => Stream::getIsEncodedList(),
            ),
            array(
                'class'=>'CButtonColumn',
            ),
        ),
    ));

    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'create',
            'caption' => Yii::t('models', 'Создать'),
            'url' => array('create'),
        )
    );

    ?>
</p>
