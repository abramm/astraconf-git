<?php

class m150203_170714_server_inputUrlProccesses extends CDbMigration
{
	public function up()
	{
		$sql = <<<SQL
ALTER TABLE `server`
ADD COLUMN `urlInputProcesses` INT(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `externalIp`;
SQL;
		$this->execute($sql);
	}

	public function down()
	{
		echo "m150203_170714_server_inputUrlProccesses does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}