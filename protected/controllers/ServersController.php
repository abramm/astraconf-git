<?php

class ServersController extends Controller
{
    public function actionIndex()
    {
        $server = new Server('search');
        $server->unsetAttributes();
        if (isset($_REQUEST['Server']))
            $server->attributes = $_REQUEST['Server'];
        $this->render('index', array('model' => $server));
    }

    public function actionView($id, $showConfig = false)
    {
        $server = self::loadModel(array('id' => $id));
        $this->render('view', array('model' => $server, 'showConfig' => $showConfig));
    }

    public function actionUpdate($id)
    {
        $server = self::loadModel(array('id' => $id));
        if (isset($_POST['Server']))
        {
            $server->attributes = $_POST['Server'];
            $server->save();
        }
        $this->render('update', array('model' => $server));
    }

    public function actionCreate()
    {
        $server = new Server();
        if (isset($_POST['Server']))
        {
            $server->attributes = $_POST['Server'];
            if ($server->save())
                $this->redirect(array('/servers/view', 'id' => $server->id));
        }
        $this->render('create', array('model' => $server));
    }

    public function actionDelete($id)
    {
        $server = self::loadModel(array('id' => $id));
        if (!$server->delete())
            throw new CHttpException(500);
    }

    public function actionGetConfigs()
    {
        $server = Server::model()->findByAttributes(array('ip' => Yii::app()->request->userHostAddress));
        /* @var $server Server */
        if (!$server)
            throw new CHttpException(403);
        $tmpName = tempnam(sys_get_temp_dir(), 'aconf');
        $zip = new ZipArchive;
        $zip->open($tmpName, ZipArchive::CREATE || ZipArchive::OVERWRITE);
        foreach ($server->adapters as $adapter)
        {
            if ($adapter->dvbTranspoder)
            {
                $fileName = 'a'.$adapter->num.'.lua';
                $zip->addFromString($fileName, $adapter->configFile);
            }
        }
        if ($server->haveRelay)
            $zip->addFromString('relay.lua', $server->relayConfigFile);
        if ($server->haveUrlInput)
            for ($i = 0; $i < $server->urlInputProcesses; $i++)
                $zip->addFromString('urlInput_'.$i.'.lua', $server->getUrlInputsConfigFile($i));
        $zip->close();
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"server.zip\"");
        echo file_get_contents($tmpName);
        unlink($tmpName);
        Yii::app()->end();
    }

    public function actionGetZabbixDiscovery()
    {
        $server = Server::model()->findByAttributes(array('ip' => Yii::app()->request->userHostAddress));
        /* @var $server Server */
        if (!$server)
            throw new CHttpException(403);
        $ret = array();
        foreach ($server->adapters as $adapter)
        {
            if ($adapter->dvbTranspoder)
                $ret[] = array(
                    '{#MAC}' => $adapter->mac,
                    '{#TRANS_NAME}' => $adapter->dvbTranspoder->description
                );
        }
        echo json_encode($ret, JSON_UNESCAPED_UNICODE);
        Yii::app()->end();
    }

    public static function loadModel($options)
    {
        $model = Server::model()->findByAttributes($options);
        if (!$model)
            throw new CHttpException(404);
        return $model;
    }

    public function filters()
    {
        return array(
            'ajaxOnly + delete',
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => array('getConfigs', 'getZabbixDiscovery')),
            array('allow', // allow authenticated users to access all actions
                'users'=>array('@'),
            ),
            array('deny'),
        );
    }
}