<?php

/**
 * This is the model class for table "stream_has_softcamServer".
 *
 * The followings are the available columns in table 'stream_has_softcamServer':
 * @property string $stream_id
 * @property string $softcamServer_id
 */
class StreamHasSoftcamServer extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stream_has_softcamServer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('stream_id, softcamServer_id', 'required'),
			array('stream_id, softcamServer_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('stream_id, softcamServer_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'stream_id' => 'Stream',
			'softcamServer_id' => 'Softcam Server',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('stream_id',$this->stream_id,true);
		$criteria->compare('softcamServer_id',$this->softcamServer_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StreamHasSoftcamServer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function findOrCreate($stream_id, $softcamServer_id)
    {
        $model = $this->findByAttributes(array(
            'stream_id' => $stream_id,
            'softcamServer_id' => $softcamServer_id,
        ));
        if (!$model)
        {
            $model = new StreamHasSoftcamServer();
            $model->stream_id = $stream_id;
            $model->softcamServer_id = $softcamServer_id;
            $model->save();
        }
        return $model;
    }
}
