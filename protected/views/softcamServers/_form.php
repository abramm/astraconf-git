<?php
/* @var $this SoftcamServerController */
/* @var $model SoftcamServer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'softcam-server-_form-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note"><?= Yii::t('models', 'Поля с {*} обязательны к заполнению', array('{*}' => '<span class="required">*</span>')); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description'); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'host'); ?>
		<?php echo $form->textField($model,'host'); ?>
		<?php echo $form->error($model,'host'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->textField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'aesKey'); ?>
		<?php echo $form->textField($model,'aesKey'); ?>
		<?php echo $form->error($model,'aesKey'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'port'); ?>
		<?php echo $form->textField($model,'port'); ?>
		<?php echo $form->error($model,'port'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'sendEMM'); ?>
        <?php echo $form->checkBox($model,'sendEMM'); ?>
        <?php echo $form->error($model,'sendEMM'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'separateConnections'); ?>
        <?php echo $form->checkBox($model,'separateConnections'); ?>
        <?php echo $form->error($model,'separateConnections'); ?>
    </div>


	<div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('app', 'Сохранить')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->