<?php

/**
 * This is the model class for table "stream".
 *
 * The followings are the available columns in table 'stream':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $serverId
 * @property string $channelId
 * @property string $type
 * @property string $dvbTranspoderId
 * @property string $isEncoded
 * @property string $biss
 * @property string $priority
 * @property string $url
 * @property string $pnr
 * @property string $casData
 * @property string $useBackup
 * @property string $filter
 *
 * The followings are the available model relations:
 * @property Server $server
 * @property Channel $channel
 * @property DvbTranspoder $dvbTranspoder
 * @property SoftcamServer[] $softcamServers
 * @property int $softcamServersCount
 *
 * @property string $typeLabel
 * @property string $isEncodedLabel
 * @property string $serverLink
 * @property string $channelLink
 * @property string $dvbTranspoderLink
 * @property string $canCast
 * @property string $internalCastUrl
 *
 * @property SoftcamServer[] $newSoftcamServers
 * @property SoftcamServer[] $oldSoftcamServers
 */
class Stream extends MyActiveRecord
{

    const TYPE_DVB = 0;
    const TYPE_URL = 1;

    const ENC_OPEN = 0;
    const ENC_CAM  = 1;
    const ENC_BISS = 2;
    const ENC_UNKNOWN = 3;
    const ENC_CI = 4;

    public $newSoftcamServers;
    public $oldSoftcamServers;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stream';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('serverId, channelId, type, isEncoded', 'required'),
			array('name, description, url', 'length', 'max'=>255),
			array('serverId, channelId, type, dvbTranspoderId, priority, pnr', 'length', 'max'=>10),
			array('isEncoded, useBackup', 'length', 'max'=>1),
			array('biss, casData', 'length', 'max'=>45),
            array('newSoftcamServers, oldSoftcamServers', 'safe'),
            array('filter', 'match','allowEmpty' => true, 'pattern' => '/^(\d*,)*\d*$/i'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, serverId, channelId, type, dvbTranspoderId, isEncoded, biss, priority, url, pnr, casData, useBackup', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'server' => array(self::BELONGS_TO, 'Server', 'serverId'),
			'channel' => array(self::BELONGS_TO, 'Channel', 'channelId'),
			'dvbTranspoder' => array(self::BELONGS_TO, 'DvbTranspoder', 'dvbTranspoderId'),
			'softcamServers' => array(self::MANY_MANY, 'SoftcamServer', 'stream_has_softcamServer(stream_id, softcamServer_id)'),
			'softcamServersCount' => array(self::STAT, 'SoftcamServer', 'stream_has_softcamServer(stream_id, softcamServer_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('models','Имя'),
			'description' => Yii::t('models','Описание'),
			'serverId' => Yii::t('models','Сервер'),
			'channelId' => Yii::t('models','Канал'),
			'type' => Yii::t('models','Тип'),
			'dvbTranspoderId' => Yii::t('models','DVB трансподер'),
			'isEncoded' => Yii::t('models','Закодирован'),
			'biss' => Yii::t('models','BISS-ключ'),
			'priority' => Yii::t('models','Приоритет'),
			'url' => Yii::t('models','URL'),
			'pnr' => Yii::t('models','PNR/SID'),
			'casData' => Yii::t('models','Параметры SoftCAM (cas_data)'),
			'useBackup' => Yii::t('models','Использовать резерв'),
            'filter' => Yii::t('models','Фильтр потоков'),
            'serverLink' => Yii::t('models','Сервер'),
			'channelLink' => Yii::t('models','Канал'),
			'dvbTranspoderLink' => Yii::t('models','Трансподер'),
			'newSoftcamServers' => Yii::t('models', 'SoftCAM-серверы'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        if ($this->newSoftcamServers)
        {
            $criteria->alias = 'stream';
            $criteria->join .= ' LEFT OUTER JOIN stream_has_softcamServer ss ON stream.id = ss.stream_id';
            $criteria->compare('ss.softcamServer_id', $this->newSoftcamServers);
        }

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('serverId',$this->serverId);
		$criteria->compare('channelId',$this->channelId);
		$criteria->compare('type',$this->type,true);
        $criteria->compare('dvbTranspoderId',$this->dvbTranspoderId);
		$criteria->compare('isEncoded',$this->isEncoded);
		$criteria->compare('biss',$this->biss,true);
		$criteria->compare('priority',$this->priority,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('pnr',$this->pnr,true);
		$criteria->compare('casData',$this->casData,true);
		$criteria->compare('useBackup',$this->useBackup);
		$criteria->compare('filter',$this->filter,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize' => 40,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Stream the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getTypesList()
    {
        return array(
            self::TYPE_DVB=>Yii::t('models', 'DVB'),
            self::TYPE_URL=>Yii::t('models', 'Прямой ввод URL'),
        );
    }

    public static function typeLabel($id)
    {
        if (!is_numeric($id))
            return $id;
        $types = self::getTypesList();
        if (isset($types[$id]))
            return $types[$id];
        else
            return null;
    }

    public function getTypeLabel()
    {
        return self::typeLabel($this->type);
    }

    public static function getIsEncodedList()
    {
        return array(
            self::ENC_OPEN=>Yii::t('models', 'Нет'),
            self::ENC_CAM =>Yii::t('models', 'Да'),
            self::ENC_BISS=>Yii::t('models', 'BISS'),
            self::ENC_UNKNOWN=>Yii::t('models', 'Да, не расшифрован'),
            self::ENC_CI=>Yii::t('models', 'Да, DVB-CI'),
        );
    }

    public static function isEncodedLabel($id)
    {
        if (!is_numeric($id))
            return $id;
        $list = self::getIsEncodedList();
        if (isset($list[$id]))
            return $list[$id];
        else
            return null;
    }

    public function getIsEncodedLabel()
    {
        return self::isEncodedLabel($this->isEncoded);
    }

    public function getServerLink()
    {
        if ($this->isNewRecord)
            return null;
        return CHtml::link($this->server->name, array('/servers/view', 'id' => $this->serverId));
    }

    public function getChannelLink()
    {
        if ($this->isNewRecord)
            return null;
        return CHtml::link($this->channel->name, array('/channels/view', 'id' => $this->channelId));
    }

    public function getDvbTranspoderLink()
    {
        if ($this->isNewRecord)
            return null;
        if ($this->type != self::TYPE_DVB)
            return null;
        return CHtml::link($this->dvbTranspoder->description, array('/transpoders/view', 'id' => $this->dvbTranspoderId));
    }

    public function getCanCast()
    {
        if ($this->channel->subscriptionLevel > Yii::app()->params['maximalSubscriptionLevel'])
            return false;
        // TODO: Rework for testing/production streaming with contract
        if ($this->isEncoded == self::ENC_CAM && $this->softcamServersCount == 0)
            return false;
        if ($this->isEncoded == self::ENC_UNKNOWN)
            return false;
        return true;
    }

    public function getInternalHTTPUrl()
    {
        $url = new AstraUrl('http', $this->server->ip, 20000 + $this->id, $this->id);
        return $url;
    }

    public function getInternalUDPUrl()
    {
        $url = new AstraUrl('udp', long2ip(ip2long('239.255.1.0') + $this->id), 1235); // 1235 for internal cast
        return $url;
    }

    public function getInternalCastUrl()
    {
        if (Yii::app()->params['useHTTPForRelay'])
            return $this->getInternalHTTPUrl()->asString();
        else
            return $this->getInternalUDPUrl()->setUdpInterface($this->server->ip)->asString();
    }

    public function afterSave()
    {
        $retval = true;
        if (!is_array($this->oldSoftcamServers))
            $this->oldSoftcamServers = array();
        if (is_array($this->newSoftcamServers))
        {
            foreach ($this->oldSoftcamServers as $oldSoftcamServerId)
            {
                $newSoftcamServersArrayKey = array_search($oldSoftcamServerId, $this->newSoftcamServers);
                if ($newSoftcamServersArrayKey !== false)
                {
                    unset ($this->newSoftcamServers[$newSoftcamServersArrayKey]);
                }
                else
                {
                    $model = StreamHasSoftcamServer::model()->deleteAllByAttributes(array('stream_id' => $this->id, 'softcamServer_id' => $oldSoftcamServerId));
                }
            }
            foreach ($this->newSoftcamServers as $newSoftcamServerId)
                StreamHasSoftcamServer::model()->findOrCreate($this->id, $newSoftcamServerId);
        }
        return $retval && parent::afterSave();
    }

    public function beforeSave()
    {
        if ($this->type != self::TYPE_DVB)
            $this->dvbTranspoderId = null;
        return parent::beforeSave();
    }

    public function delete()
    {
        $retval = false;
        $useTransaction = !$this->dbConnection->currentTransaction;
        if ($useTransaction)
            $transaction = $this->dbConnection->beginTransaction();
        try
        {
            StreamHasSoftcamServer::model()->deleteAllByAttributes(array('stream_id' => $this->id, ));
            $retval = parent::delete();
            if ($useTransaction)
                $transaction->commit();
        }
        catch(Exception $e)
        {
            Yii::log('Error deleting stream '.$this->id.', caught exception: '.$e->getMessage(), CLogger::LEVEL_ERROR);
            if ($useTransaction)
                $transaction->rollback();
        }
        return $retval;
    }

}
