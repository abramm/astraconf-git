<?php
/* @var $this TranspodersController */
/* @var $model DvbTOptions */
/* @var $form CActiveForm */
?>

<div class="form">

    <h2><?= Yii::t('menu', 'Опции DVB-T/T2');?></h2>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'frequency'); ?>
        <?php echo $form->textField($model,'frequency'); ?>
        <?php echo $form->error($model,'frequency'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'bandwidth'); ?>
        <?php echo $form->textField($model,'bandwidth'); ?>
        <?php echo $form->error($model,'bandwidth'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'guardinterval'); ?>
        <?php echo $form->textField($model,'guardinterval'); ?>
        <?php echo $form->error($model,'guardinterval'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'transmitmode'); ?>
        <?php echo $form->textField($model,'transmitmode'); ?>
        <?php echo $form->error($model,'transmitmode'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'hierarchy'); ?>
        <?php echo $form->textField($model,'hierarchy'); ?>
        <?php echo $form->error($model,'hierarchy'); ?>
    </div>

</div><!-- form -->