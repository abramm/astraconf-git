<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
$conf = array(
    // application components
    'components'=>array(
        'log'=>array(
            'routes'=>array(
                // add here
            ),
        ),
    ),
);

$dbConf = require('db.php');
$commonConf = require('common.php');

$conf = CMap::mergeArray($conf, $dbConf);
$conf = CMap::mergeArray($conf, $commonConf);

return $conf;