<?php

class m131126_122007_eventDatabase extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
CREATE  TABLE IF NOT EXISTS `monitoringEvent` (
  `id` INT(11) NOT NULL ,
  `channelId` INT(10) UNSIGNED NOT NULL ,
  `streamId` INT(10) UNSIGNED NOT NULL ,
  `softcamServerId` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `timestamp` INT(20) UNSIGNED NOT NULL ,
  `pes_error` INT(20) UNSIGNED NOT NULL ,
  `cc_error` INT(20) UNSIGNED NOT NULL ,
  `bitrate` INT(20) UNSIGNED NOT NULL ,
  `scrambled` INT(1) UNSIGNED NOT NULL ,
  `onair` INT(1) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_monitoringData_channel1_idx` (`channelId` ASC) ,
  INDEX `fk_monitoringEvent_stream1_idx` (`streamId` ASC) ,
  INDEX `fk_monitoringEvent_softcamServer1_idx` (`softcamServerId` ASC) ,
  CONSTRAINT `fk_monitoringData_channel1`
    FOREIGN KEY (`channelId` )
    REFERENCES `channel` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_monitoringEvent_stream1`
    FOREIGN KEY (`streamId` )
    REFERENCES `stream` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_monitoringEvent_softcamServer1`
    FOREIGN KEY (`softcamServerId` )
    REFERENCES `softcamServer` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
SQL;
        $this->execute($sql);
	}

	public function down()
	{
		echo "m131126_122007_eventDatabase does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}