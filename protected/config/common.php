<?php

$commonConf = array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Astra Streaming Software Configuration Tool',

    // preloading 'log' component
    'preload'=>array('log'),

    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
    ),

    'components'=>array(

        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
            ),
        ),

        'mosaicCache' => array(
            'class' => 'CMemCache',
            'servers'=>array(
                array(
                    'host'=>'localhost',
                    'port'=>11211,
                ),
            ),
            'keyPrefix' => 'astraConf',
            'hashKey' => false,
        ),

    ),

    'params'=>array(
        'useMACAddressInConfig'=>false,
        'useHTTPForRelay' => false,
        'udpAnalyzeInterface' => null,
        'mosaicRefreshInterval' => 120,
        'mosaicImageFormat' => 'gif', // may be gif of png
        'mosaicGifFrames' => 5,
        'maxMonitoringEventsAge' => 300,
        'monitoringEventTimeout' => 60,
        'administrators' => array(
        ),
        'tvGenres' => array(
        ),
        'playlistName' => 'Astraconf Generated Playlist',
        'maximalSubscriptionLevel' => 10,
        'DVBBufferSize' => null,
        'strictMode' => false,
    ),
);

$userConf = require(dirname(__FILE__).'/../../config/common.php');
return CMap::mergeArray($commonConf, $userConf);