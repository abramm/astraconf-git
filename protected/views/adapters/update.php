<?php
/**
 * @var $this AdaptersController
 * @var $model Adapter
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список адаптеров') => array('/adapters/index'),
);

$this->pageHeader = Yii::t('menu','Редактирование адаптера {NAME}', array('{NAME}' => $model->displayName));
?>
<p>
    <?php
    $this->renderPartial('_form', array('model' => $model));
    ?>
</p>
