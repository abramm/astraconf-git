<?php
/**
 * @var $this ServersController
 * @var $model Server
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список серверов') => array('/servers/index'),
);

$this->pageHeader = Yii::t('menu','Редактирование сервера {NAME}', array('{NAME}' => $model->name));
?>
<p>
    <?php
    $this->renderPartial('_form', array('model' => $model));
    ?>
</p>
