<?php

class m131202_095401_softCam_multiple extends CDbMigration
{
	public function up()
	{
        $this->execute('ALTER TABLE `softcamServer` ADD COLUMN `separateConnections` INT(1) UNSIGNED NOT NULL DEFAULT 0  AFTER `sendEMM` ;');
	}

	public function down()
	{
		echo "m131202_095401_softCam_multiple does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
