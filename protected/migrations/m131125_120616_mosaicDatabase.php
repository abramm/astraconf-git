<?php

class m131125_120616_mosaicDatabase extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
ALTER TABLE `channel` ADD COLUMN `oldMosaicImageFileName` VARCHAR(255) NOT NULL DEFAULT ''  AFTER `serverId` , ADD COLUMN `newMosaicImageFileName` VARCHAR(255) NOT NULL DEFAULT ''  AFTER `oldMosaicImageFileName` , ADD COLUMN `lastMosaicImageUpdate` INT(20) UNSIGNED NOT NULL DEFAULT 0  AFTER `newMosaicImageFileName`;
SQL;
        $this->execute($sql);
	}

	public function down()
	{
		echo "m131125_120616_mosaicDatabase does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}