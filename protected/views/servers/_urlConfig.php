<?php
/**
 * @var $server Server
 * @var $this Controller
 * @var $num int
 */

$streams = $server->streams;


?>#!/usr/bin/env astra-4.0

if (init_all ~= nil) then -- check if monitoring is available
    event_request = "<?= $this->createAbsoluteUrl('/monitoring/event') ?>"
    event_request_interval = <?= Yii::app()->params['monitoringEventTimeout'] / 2 ?>
    init_all()
end

pidfile("/var/run/astra_urlInput_<?= $num ?>.pid")
log.set({filename = "/var/log/astra/urlInput_<?= $num ?>.log"})

<?php

$i = 0;

foreach ($streams as $stream)
{
    if ($stream->canCast && $stream->type == Stream::TYPE_URL && ($i % $server->urlInputProcesses == $num))
    {
        ?>
make_channel({ -- <?= $stream->name ?> --
    name = "<?= $stream->channel->directCast ? $stream->channel->name : $stream->name ?>",
<?php
        if ($stream->channel->directCast)
        {
            ?>
    id = <?= $stream->channelId ?>,
    event = true,
<?php } ?>
    input = {
<?php
        echo '        "'.AstraUrl::fromString($stream->url)->addParam('id='.$stream->id)->asString().'", -- '.$stream->name.PHP_EOL;
        if ($stream->channel->directCast && $stream->useBackup)
            echo '        "file:///home/abram/matras.ts#loop&id=fallback",';
        ?>

    },

output = {
<?php
        if ($stream->channel->directCast)
        {
            if ($stream->channel->server->enableHttpOutput)
                echo '        "', $stream->channel->outputHttpUrl, '",', PHP_EOL;
            if ($stream->channel->server->enableUdpOutput)
                echo '        "', $stream->channel->outputUdpUrl, '",', PHP_EOL;
        }
        else
            echo '        "', $stream->internalCastUrl, '",', PHP_EOL;
        ?>
    } -- output
})

    <?php
    }
    $i++;
}

?>
