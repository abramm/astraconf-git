<?php
/**
 * @var $server Server
 */

$channels = $server->channels;


?>#!/usr/bin/env astra-4.0

if (init_all ~= nil) then -- check if monitoring is available
    event_request = "<?= $this->createAbsoluteUrl('/monitoring/event') ?>"
    event_request_interval = <?= Yii::app()->params['monitoringEventTimeout'] / 2 ?>
    init_all()
end

pidfile("/var/run/astra_relay.pid")
log.set({filename = "/var/log/astra/relay.log"})

<?php


foreach ($channels as $channel)
{
    if ($channel->canCast && !$channel->directCast)
    {
    ?>
make_channel({ -- <?= $channel->name ?> --
    name = "<?= $channel->name ?>",
    id = <?= $channel->id ?>,
    event = true,
    input = {
<?php
        foreach ($channel->streams as $stream)
        {
            if ($stream->canCast)
            {
                echo '        "'.AstraUrl::fromString($stream->internalCastUrl)->setUdpInterface($server->ip)->addParam('id='.$stream->id)->asString().'", -- '.$stream->name.PHP_EOL;
            }
        }
        // @todo: Смотреть во все, без useBackup ставить в конец
        if ($stream->useBackup)
        {
            echo '        "file:///home/abram/matras.ts#loop&id=fallback",', PHP_EOL;
        }
?>
    },

    output = {
<?php

        if ($stream->channel->server->enableHttpOutput)
            echo '        "', $stream->channel->outputHttpUrl, '",', PHP_EOL;
        if ($stream->channel->server->enableUdpOutput)
            echo '        "', $stream->channel->outputUdpUrl, '",', PHP_EOL;

        ?>
    } -- output
})

<?php
    }
}

?>
