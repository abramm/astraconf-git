<?php

class TranspodersController extends Controller
{
    public function actionIndex()
    {
        $transpoder = new DvbTranspoder('search');
        $transpoder->unsetAttributes();
        if (isset($_REQUEST['DvbTranspoder']))
            $transpoder->attributes = $_REQUEST['DvbTranspoder'];
        $this->render('index', array('model' => $transpoder));
    }

    public function actionView($id)
    {
        $transpoder = self::loadModel(array('id' => $id));
        $this->render('view', array('model' => $transpoder));
    }

    public function actionUpdate($id)
    {
        $transpoder = self::loadModel(array('id' => $id));
        if (isset($_POST['DvbTranspoder']))
        {
            $transpoder->attributes = $_POST['DvbTranspoder'];
            $transpoder->save();

            if (isset($_POST['DvbSOptions']))
            {
                $dvbSOptions = $transpoder->dvbSOptions;
                if (!$dvbSOptions)
                {
                    $dvbSOptions = new DvbSOptions();
                    $dvbSOptions->dvbTranspoderId = $transpoder->id;
                }
                $dvbSOptions->attributes = $_POST['DvbSOptions'];
                $dvbSOptions->save();
            }

            if (isset($_POST['DvbTOptions']))
            {
                $dvbTOptions = $transpoder->dvbTOptions;
                if (!$dvbTOptions)
                {
                    $dvbTOptions = new DvbTOptions();
                    $dvbTOptions->dvbTranspoderId = $transpoder->id;
                }
                $dvbTOptions->attributes = $_POST['DvbTOptions'];
                $dvbTOptions->save();
            }

            if (isset($_POST['DvbCOptions']))
            {
                $dvbCOptions = $transpoder->dvbCOptions;
                if (!$dvbCOptions)
                {
                    $dvbCOptions = new DvbCOptions();
                    $dvbCOptions->dvbTranspoderId = $transpoder->id;
                }
                $dvbCOptions->attributes = $_POST['DvbCOptions'];
                $dvbCOptions->save();
            }
        }

        $this->render('update', array('model' => $transpoder));
    }

    public function actionCreate()
    {
        $transpoder = new DvbTranspoder();
        if (isset($_POST['DvbTranspoder']))
        {
            $transpoder->attributes = $_POST['DvbTranspoder'];
            if ($transpoder->save())
            {
                $redirect = false;
                if (isset($_POST['DvbSOptions']))
                {
                    $dvbSOptions = $transpoder->dvbSOptions;
                    if (!$dvbSOptions)
                    {
                        $dvbSOptions = new DvbSOptions();
                        $dvbSOptions->dvbTranspoderId = $transpoder->id;
                    }
                    $dvbSOptions->attributes = $_POST['DvbSOptions'];
                    if ($dvbSOptions->save())
                        $redirect = true;
                }

                if (isset($_POST['DvbTOptions']))
                {
                    $dvbTOptions = $transpoder->dvbTOptions;
                    if (!$dvbTOptions)
                    {
                        $dvbTOptions = new DvbTOptions();
                        $dvbTOptions->dvbTranspoderId = $transpoder->id;
                    }
                    $dvbTOptions->attributes = $_POST['DvbTOptions'];
                    if($dvbTOptions->save())
                        $redirect = true;
                }
                if ($redirect)
                    $this->redirect(array('/transpoders/view', 'id' => $transpoder->id));
            }
        }
        $this->render('create', array('model' => $transpoder));
    }

    public function actionDelete($id)
    {
        $transpoder = self::loadModel(array('id' => $id));
        if (!$transpoder->delete())
            throw new CHttpException(500);
    }

    /**
     * @param $options
     * @return DvbTranspoder
     * @throws CHttpException
     */
    public static function loadModel($options)
    {
        $model = DvbTranspoder::model()->findByAttributes($options);
        if (!$model)
            throw new CHttpException(404);
        return $model;
    }

    public function filters()
    {
        return array(
            'ajaxOnly + delete',
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated users to access all actions
                'users'=>array('@'),
            ),
            array('deny'),
        );
    }

}