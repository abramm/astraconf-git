<?php
/**
 * @var $this AdaptersController
 * @var $model Adapter
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список адаптеров') => array('/adapters/index'),
);

$this->pageHeader = Yii::t('menu','Создание адаптера');
?>
<p>
    <?php
    $this->renderPartial('_form', array('model' => $model));
    ?>
</p>
