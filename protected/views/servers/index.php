<?php
/**
 * @var $this ServersController
 * @var $model Server
 */

$this->pageHeader = Yii::t('menu','Список серверов');
?>
<p>
    <?php
    $dataProvider=$model->search();

    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'columns' => array(
            'name',
            'ip',
            array(
                'name' => 'outputsList',
                'filter' => false,
            ),
            'urlInputProcesses',
            array(
                'class'=>'CButtonColumn',
            ),
        ),
    ));

    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'create',
            'caption' => Yii::t('models', 'Создать'),
            'url' => array('create'),
        )
    );

    ?>
</p>
