<?php

class m140324_120245_stream_filter extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
ALTER TABLE `stream` ADD COLUMN `filter` VARCHAR(255) NULL DEFAULT NULL  AFTER `useBackup`;
SQL;
        $this->execute($sql);
	}

	public function down()
	{
		echo "m140324_120245_stream_filter does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}