<?php

class AdaptersController extends Controller
{
    public function actionIndex()
    {
        $adapter = new Adapter('search');
        $adapter->unsetAttributes();
        if (isset($_REQUEST['Adapter']))
            $adapter->attributes = $_REQUEST['Adapter'];
        $this->render('index', array('model' => $adapter));
    }

    public function actionView($id, $showConfig = false)
    {
        $adapter = self::loadModel(array('id' => $id));
        $this->render('view', array('model' => $adapter, 'showConfig' => $showConfig));
    }

    public function actionUpdate($id)
    {
        $adapter = self::loadModel(array('id' => $id));
        if (isset($_POST['Adapter']))
        {
            $adapter->attributes = $_POST['Adapter'];
            if (!$adapter->device)
                $adapter->device = null;
            $adapter->save();
        }
        $this->render('update', array('model' => $adapter));
    }

    public function actionCreate()
    {
        $adapter = new Adapter();
        if (isset($_POST['Adapter']))
        {
            $adapter->attributes = $_POST['Adapter'];
            if ($adapter->save())
                $this->redirect(array('/adapters/view', 'id' => $adapter->id));
        }
        $this->render('create', array('model' => $adapter));
    }

    public function actionDelete($id)
    {
        $adapter = self::loadModel(array('id' => $id));
        if (!$adapter->delete())
            throw new CHttpException(500);
    }

    /**
     * @param $options
     * @return Adapter
     * @throws CHttpException
     */
    public static function loadModel($options)
    {
        $model = Adapter::model()->findByAttributes($options);
        if (!$model)
            throw new CHttpException(404);
        return $model;
    }

    public function filters()
    {
        return array(
            'ajaxOnly + delete',
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated users to access all actions
                'users'=>array('@'),
            ),
            array('deny'),
        );
    }

}