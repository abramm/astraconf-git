<?php

/**
 * This is the model class for table "dvbTOptions".
 *
 * The followings are the available columns in table 'dvbTOptions':
 * @property string $id
 * @property string $dvbTranspoderId
 * @property string $frequency
 * @property string $bandwidth
 * @property string $guardinterval
 * @property string $transmitmode
 * @property string $hierarchy
 *
 * The followings are the available model relations:
 * @property DvbTranspoder $dvbTranspoder
 */
class DvbTOptions extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dvbTOptions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dvbTranspoderId, frequency', 'required'),
			array('dvbTranspoderId, frequency', 'length', 'max'=>10),
			array('bandwidth, guardinterval, transmitmode, hierarchy', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dvbTranspoderId, frequency, bandwidth, guardinterval, transmitmode, hierarchy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dvbTranspoder' => array(self::BELONGS_TO, 'DvbTranspoder', 'dvbTranspoderId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dvbTranspoderId' => Yii::t('models', 'Трансподер'),
			'frequency' => Yii::t('models', 'Частота несущей'),
			'bandwidth' => Yii::t('models', 'Ширина полосы вокруг несущей'),
			'guardinterval' => Yii::t('models', 'Длина защитного интервала'),
			'transmitmode' => Yii::t('models', 'Режим модуляции'),
			'hierarchy' => Yii::t('models', 'Использование иерархического кодирования'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dvbTranspoderId',$this->dvbTranspoderId);
		$criteria->compare('frequency',$this->frequency,true);
		$criteria->compare('bandwidth',$this->bandwidth,true);
		$criteria->compare('guardinterval',$this->guardinterval,true);
		$criteria->compare('transmitmode',$this->transmitmode,true);
		$criteria->compare('hierarchy',$this->hierarchy,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DvbTOptions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
