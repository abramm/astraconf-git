<?php


class AstraConfigDvbTune {

    public $name;
    public $comment;
    public $params;

    public function __construct($name = null, $comment = null, $params = [])
    {
        $this->name = $name;
        $this->comment = $comment;
        $this->params = $params;
    }

    public function asConfArray()
    {
        $retval = [];
        $retval['start'] = $this->name.' = dvb_tune({';
        $retval['comment'] = $this->comment;
        $retval['end'] = '})';
        $retval['content'] = [];
        foreach($this->params as $param => $value)
        {
            $type = gettype($value);
            switch ($type)
            {
                case 'boolean':
                    $retval['content'][$param] = $param.' = '.($value ? 'true' : 'false').',';
                    break;
                case 'integer':
                    $retval['content'][$param] = $param.' = '.$value.',';
                    break;
                case 'string':
                    $retval['content'][$param] = $param.' = "'.addslashes($value).'",';
                    break;
                case 'NULL':
                    break;
                default:
                    throw new Exception('Unsupported type '.$type.' of param '.$param);
            }
        }
        return $retval;
    }

}