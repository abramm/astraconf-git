<?php
/**
 * @var $this MosaicController
 * @var $level int
 */

$this->pageHeader = Yii::t('menu','Мозаика каналов');
?>
    <p class="mosaics">
        <?php

        $channelSearch = new Channel('search');
        $channelSearch->unsetAttributes();
        if ($level)
            $channelSearch->subscriptionLevel = '<='.$level;
        $channelSearch->canCastSearch = 1;
        $dataProvider = $channelSearch->search();
        $dataProvider->pagination = false;
        $dataProvider->criteria->order = 'name';

        foreach($dataProvider->getData() as $channel)
        {
            $this->renderPartial('_box', array('channel' => $channel, 'mayRefresh' => false));
        }

        ?>
    </p>
<?php

$levels = Yii::app()->db->createCommand('SELECT  `subscriptionLevel`
FROM  `channel`
GROUP BY  `subscriptionLevel`
ORDER BY  `subscriptionLevel` ASC ;')->queryColumn();

$items = array();
foreach ($levels as $l)
{
    $items[$l] = $l;
}

echo CHtml::beginForm($this->createUrl($this->route), 'get');
echo CHtml::label(Yii::t('models', 'Тариф'),'level');
echo CHtml::dropDownList('level', $level, $items);
foreach ($_GET as $k => $v)
{
    if ($k != 'r' && $k != 'level')
        echo CHtml::hiddenField($k, $v);
}
echo CHtml::submitButton(Yii::t('models', 'Фильтровать'), array('id' => null, 'name' => null));
echo CHtml::endForm();