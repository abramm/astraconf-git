<?php
/**
 * @var $this AdaptersController
 * @var $model Adapter
 * @var $showConfig bool
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список адаптеров') => array('/adapters/index'),
);

$this->pageHeader = Yii::t('menu','Просмотр адаптера {NAME}', array('{NAME}' => $model->displayName));
?>
    <p>
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'id',
                'serverLink:html',
                'num',
                'device',
                'mac',
                'inUse',
            ),
        ));
        ?>
    </p>
<?php

$this->widget('zii.widgets.jui.CJuiButton',array(
        'buttonType'=>'link',
        'name' => 'update',
        'caption' => Yii::t('models', 'Редактировать'),
        'url' => array('update', 'id' => $model->id),
    )
);

if ($showConfig)
{
    ?>
    <h2><?= Yii::t('menu', 'Конфигурация'); ?></h2>
    <?php
    echo CHtml::textArea('astraConfig', $model->configFile, array('rows' => 25, 'cols' => 80, 'readonly'=>true));

}
else
{
    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'showConfig',
            'caption' => Yii::t('models', 'Показать конфигурацию'),
            'url' => array('/adapters/view', 'id' => $model->id, 'showConfig' => true),
        )
    );
}