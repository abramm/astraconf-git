<?php
/**
 * @var $this PlaylistController
 * @var $type int
 * @var $text string
 */

$this->pageHeader = Yii::t('menu','Просмотр плейлиста');
?>
<p>
    <?php
    echo CHtml::tag('h3', array(), Yii::t('app', 'Плейлист'));
    echo CHtml::textArea('playlist', $text, array('rows' => 50, 'cols' => 80, 'readonly'=>true));
    echo '<br />';
    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'download',
            'caption' => Yii::t('models', 'Скачать'),
            'url' => array('download', 'type' => $type),
        )
    );

    ?>
</p>
