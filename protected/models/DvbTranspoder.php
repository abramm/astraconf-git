<?php

/**
 * This is the model class for table "dvbTranspoder".
 *
 * The followings are the available columns in table 'dvbTranspoder':
 * @property string $id
 * @property string $description
 * @property string $satellite
 * @property string $adapterId
 * @property string $type
 * @property string $budget
 * @property string $modulation
 * @property string $fec
 *
 * The followings are the available model relations:
 * @property DvbSOptions $dvbSOptions
 * @property DvbTOptions $dvbTOptions
 * @property DvbCOptions $dvbCOptions
 * @property Adapter $adapter
 * @property Stream[] $streams
 * @property int $streamsCount
 */
class DvbTranspoder extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dvbTranspoder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('adapterId', 'required'),
			array('description, satellite, modulation, fec', 'length', 'max'=>45),
			array('adapterId', 'length', 'max'=>10),
			array('type', 'length', 'max'=>5),
			array('budget', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, description, satellite, adapterId, type, budget, modulation, fec', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dvbSOptions' => array(self::HAS_ONE, 'DvbSOptions', 'dvbTranspoderId'),
			'dvbTOptions' => array(self::HAS_ONE, 'DvbTOptions', 'dvbTranspoderId'),
			'dvbCOptions' => array(self::HAS_ONE, 'DvbCOptions', 'dvbTranspoderId'),
			'adapter' => array(self::BELONGS_TO, 'Adapter', 'adapterId'),
            'streams' => array(self::HAS_MANY, 'Stream', 'dvbTranspoderId'),
            'streamsCount' => array(self::STAT, 'Stream', 'dvbTranspoderId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => Yii::t('models','Описание'),
			'satellite' => Yii::t('models','Спутник'),
			'adapterId' => Yii::t('models','Адаптер'),
			'adapterLink' => Yii::t('models','Адаптер'),
			'type' => Yii::t('models','Тип'),
			'budget' => Yii::t('models','Аппаратная фильтрация'),
			'modulation' => Yii::t('models','Модуляция'),
			'fec' => Yii::t('models','FEC'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('satellite',$this->satellite,true);
		$criteria->compare('adapterId',$this->adapterId,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('budget',$this->budget,true);
		$criteria->compare('modulation',$this->modulation,true);
		$criteria->compare('fec',$this->fec,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize' => 40,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DvbTranspoder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getAdapterLink()
    {
        if ($this->isNewRecord)
            return null;
        return CHtml::link($this->adapter->displayName, array('/adapters/view', 'id' => $this->adapterId));
    }

    public static function getPossibleDVBTypes()
    {
        return array(
            'S' => 'S',
            'S2' => 'S2',
            'T' => 'T',
            'T2' => 'T2',
            'C' => 'C',
        );
    }

    public function delete()
    {
        $retval = false;
        $useTransaction = !$this->dbConnection->currentTransaction;
        if ($useTransaction)
            $transaction = $this->dbConnection->beginTransaction();
        try
        {
            foreach ($this->streams as $stream)
            {
                if (!$stream->delete())
                    throw new CException('Error deleting stream '.$stream->id);
            }
            if ($this->dvbTOptions && !$this->dvbTOptions->delete())
                throw new CException('Error deleting DVB-T options for transpoder '.$this->id);
            if ($this->dvbSOptions && !$this->dvbSOptions->delete())
                throw new CException('Error deleting DVB-S options for transpoder '.$this->id);
            if ($this->dvbCOptions && !$this->dvbCOptions->delete())
                throw new CException('Error deleting DVB-C options for transpoder '.$this->id);
            $retval = parent::delete();
            if ($useTransaction)
                $transaction->commit();
        }
        catch(Exception $e)
        {
            Yii::log('Error deleting channel '.$this->id.', caught exception: '.$e->getMessage(), CLogger::LEVEL_ERROR);
            if ($useTransaction)
                $transaction->rollback();
        }
        return $retval;
    }
}
