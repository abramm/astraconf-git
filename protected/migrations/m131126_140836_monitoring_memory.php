<?php

class m131126_140836_monitoring_memory extends CDbMigration
{
	public function up()
	{
        $this->execute('

DROP TABLE IF EXISTS `monitoringEvent`;
CREATE TABLE `monitoringEvent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channelId` int(10) unsigned NOT NULL,
  `streamId` int(10) unsigned DEFAULT NULL,
  `softcamServerId` int(10) unsigned DEFAULT NULL,
  `timestamp` int(20) unsigned NOT NULL,
  `pes_error` int(20) unsigned DEFAULT NULL,
  `cc_error` int(20) unsigned DEFAULT NULL,
  `bitrate` int(20) unsigned DEFAULT NULL,
  `scrambled` int(1) unsigned NOT NULL,
  `onair` int(1) unsigned NOT NULL,
  `fallback` int(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_monitoringData_channel1_idx` (`channelId`),
  KEY `fk_monitoringEvent_stream1_idx` (`streamId`),
  KEY `fk_monitoringEvent_softcamServer1_idx` (`softcamServerId`),
  CONSTRAINT `fk_monitoringData_channel1` FOREIGN KEY (`channelId`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_monitoringEvent_softcamServer1` FOREIGN KEY (`softcamServerId`) REFERENCES `softcamServer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_monitoringEvent_stream1` FOREIGN KEY (`streamId`) REFERENCES `stream` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=MEMORY  DEFAULT CHARSET=utf8;


        ');
	}

	public function down()
	{
		echo "m131126_140836_monitoring_memory does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}