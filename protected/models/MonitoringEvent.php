<?php

/**
 * This is the model class for table "monitoringEvent".
 *
 * The followings are the available columns in table 'monitoringEvent':
 * @property integer $id
 * @property string $channelId
 * @property string $streamId
 * @property string $softcamServerId
 * @property string $timestamp
 * @property string $pes_error
 * @property string $cc_error
 * @property string $bitrate
 * @property string $scrambled
 * @property string $onair
 * @property string $fallback
 *
 * The followings are the available model relations:
 * @property Channel $channel
 * @property Stream $stream
 * @property SoftcamServer $softcamServer
 *
 * Virtual attributes:
 * @property string $input_id
 * @property string $channel_id
 * @property string $overallState
 * @property string $overallStateString
 */
class MonitoringEvent extends MyActiveRecord
{

    const STATE_UNKNOWN = 0;
    const STATE_WORKING = 1;
    const STATE_WARNING = 2;
    const STATE_ERROR = 3;
    const STATE_SCRAMBLED = 4;
    const STATE_FALLBACK = 5;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'monitoringEvent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('channelId, timestamp, scrambled, onair, fallback', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('channelId, streamId, softcamServerId', 'length', 'max'=>10),
			array('timestamp, pes_error, cc_error, bitrate', 'length', 'max'=>20),
			array('scrambled, onair, fallback', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, channelId, streamId, softcamServerId, timestamp, pes_error, cc_error, bitrate, scrambled, onair', 'safe', 'on'=>'search'),
            array('input_id, channel_id', 'safe', 'on' => 'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'channel' => array(self::BELONGS_TO, 'Channel', 'channelId'),
			'stream' => array(self::BELONGS_TO, 'Stream', 'streamId'),
			'softcamServer' => array(self::BELONGS_TO, 'SoftcamServer', 'softcamServerId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'channelId' => 'Channel',
			'streamId' => 'Stream',
			'softcamServerId' => 'Softcam Server',
			'timestamp' => 'Timestamp',
			'pes_error' => 'Pes Error',
			'cc_error' => 'Cc Error',
			'bitrate' => 'Bitrate',
			'scrambled' => 'Scrambled',
			'onair' => 'Onair',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('channelId',$this->channelId,true);
		$criteria->compare('streamId',$this->streamId,true);
		$criteria->compare('softcamServerId',$this->softcamServerId,true);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('pes_error',$this->pes_error,true);
		$criteria->compare('cc_error',$this->cc_error,true);
		$criteria->compare('bitrate',$this->bitrate,true);
		$criteria->compare('scrambled',$this->scrambled,true);
		$criteria->compare('onair',$this->onair,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MonitoringEvent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function setInput_id($val)
    {
        $this->fallback = 0;
        $parts = explode('_', $val);
        if ($val == 'fallback')
        {
            $this->onair = 0;
            $this->fallback = 1;
        }
        elseif (is_numeric($val) == 1)
        {
            $this->streamId = intval($val);
        }
        elseif (count($parts) == 3 && $parts[1] == 'cam')
        {
            $this->streamId = intval($parts[0]);
            $this->softcamServerId = intval($parts[2]);
        }
        else
        {
            Yii::log('Could not parse event input_id "'.$val.'"', CLogger::LEVEL_ERROR);
        }
    }

    public function setChannel_id($val)
    {
        $this->channelId = $val;
    }

    public function beforeSave()
    {
        if ($this->onair && !$this->fallback && $this->stream->channelId != $this->channelId)
        {
            Yii::log('Could not save monitoring event: stream '.$this->streamId.' does not belongs to channel '.$this->channelId);
            $this->errors['streamId'] = 'Stream '.$this->streamId.' does not belongs to channel '.$this->channelId;
            return false;
        }
        else
            return parent::beforeSave();
    }

    public function afterSave()
    {
        if (rand(0,1000) == 1)
            return self::cleanup() && parent::afterSave();
        else
            return parent::afterSave();
    }

    public static function cleanup()
    {
        return self::model()->deleteAll('`timestamp` < :time', array(':time' => time() - 15 - Yii::app()->params['maxMonitoringEventsAge']));
    }

    public function getOverallState()
    {
        if ($this->onair)
        {
            if ($this->fallback)
                return self::STATE_FALLBACK;
            if ($this->scrambled)
                return self::STATE_SCRAMBLED;
            if ($this->bitrate < 100 || $this->pes_error > 0 || $this->cc_error > 0) // @todo: Check last 30 seconds
                return self::STATE_WARNING;
            return self::STATE_WORKING;
        }
        else
            return self::STATE_ERROR;
        return self::STATE_UNKNOWN;
    }

    public function getOverallStateString()
    {
        switch ($this->overallState)
        {
            case self::STATE_WORKING:
                return Yii::t('monitoring', 'Работает');
            case self::STATE_WARNING:
                return Yii::t('monitoring', 'Есть ошибки');
            case self::STATE_ERROR:
                return Yii::t('monitoring', 'Не работает');
            case self::STATE_SCRAMBLED:
                return Yii::t('monitoring', 'Закодирован');
            case self::STATE_FALLBACK:
                return Yii::t('monitoring', 'Матрац');
            case self::STATE_UNKNOWN:
            default:
                return Yii::t('monitoring', 'Неизвестно');
        }
    }
}
