<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="content">
    <h1><?php echo $this->pageHeader; ?></h1>
	<?php echo $content; ?>
</div><!-- content -->
<?php $this->endContent(); ?>