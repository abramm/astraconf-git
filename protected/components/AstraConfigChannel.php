<?php


class AstraConfigChannel {

    public $inputUrls = [];
    public $outputUrls = [];
    /**
     * @var AstraConfig
     */
    public $parent;
    /**
     * @var Channel
     */
    public $channel;
    public $comment;

    public function __construct($parent, $channel, $comment)
    {
        if (!is_a($parent, 'AstraConfig'))
            throw new Exception('Wrong parent');
        if (!is_a($channel, 'Channel'))
            throw new Exception('Wrong class, Channel expected');
        $this->parent = $parent;
        $this->channel = $channel;
        $this->comment = $comment;
    }

    /**
     * @param $stream Stream
     * @throws Exception
     */
    public function addInput($stream)
    {
        $camParams = [];
        $url = new AstraUrl();
        switch ($stream->isEncoded)
        {
            case Stream::ENC_OPEN:
                break;
            case Stream::ENC_CI:
                $camParams[] = ['cam'];
                break;
            case Stream::ENC_BISS:
                $camParams[] = ['biss='.$stream->biss];
                break;
            case Stream::ENC_CAM:
                foreach($stream->softcamServers as $server)
                {
                    $softcamName = $server->separateConnections ?
                        'cam_'.$server->id.'_for_'.$stream->id :
                        'cam_'.$server->id;
                    $this->parent->addNewcamdConf($softcamName, $server);
                    $param = ['cam='.$softcamName];
                    if ($stream->casData)
                        $param[]='cas_data='.$stream->casData;
                    $camParams[] = $param;
                }
                break;
            case Stream::ENC_UNKNOWN:
                return false;
                break;
            default:
                throw new Exception('Unknown encoding');
        }
        switch ($stream->type)
        {
            case Stream::TYPE_DVB:
                $dvbAdapter = $stream->dvbTranspoder->adapter;
                $dvbName = 'dvb_'.$dvbAdapter->id;
                $this->parent->addDvbTune($dvbName, $dvbAdapter);
                $url = new AstraUrl('dvb', $dvbName);
                break;
            case Stream::TYPE_URL:
                $url = AstraUrl::fromString($stream->url);
                break;
            default:
                break;
        }
        if ($stream->pnr)
            $url->params[] = 'pnr='.$stream->pnr;
        if ($stream->filter && strlen($stream->filter) > 0)
            $url->params[] = 'filter='.$stream->filter;
        $url->params[] = 'id='.$stream->id;

        if (count($camParams) > 0)
        {
            foreach ($camParams as $cam)
            {
                $myUrl = clone $url;
                $myUrl->params = CMap::mergeArray($url->params, $cam);
                $this->inputUrls[] = $myUrl->asString();
            }
        }
        else
        {
            $this->inputUrls[] = $url->asString();
        }
    }

    public function addFallback()
    {
        $this->inputUrls[] = 'file:///home/abram/matras.ts#loop&id=fallback';
    }

    public function asConfArray()
    {
        $retval = [];
        $retval['start'] = 'make_channel({';
        $retval['comment'] = $this->comment;
        $retval['end'] = '})';

        $input = [
            'start' => 'input = {',
            'comment' => Yii::t('app', 'ввод'),
            'end' => '},',
            'content' => '',
        ];

        $inputUrls = [];
        foreach ($this->inputUrls as $url)
        {
            $inputUrls[] = '"'.addslashes($url).'"';
        }
        $input['content'] = implode(','.PHP_EOL,$inputUrls);

        $output = [
            'start' => 'output = {',
            'comment' => Yii::t('app', 'вывод'),
            'end' => '}',
            'content' => '',
        ];

        $outputUrls = [];
        foreach ($this->outputUrls as $url)
        {
            $outputUrls[] = '"'.addslashes($url).'"';
        }
        $output['content'] = implode(','.PHP_EOL,$outputUrls);

        $retval['content'] = [
            'name = "'.addslashes($this->channel->name).'",',
            'id = '.$this->channel->id.',',
            'event = true,',
            $input,
            $output,
        ];

        return $retval;
    }

}