<?php

class StreamsController extends Controller
{
    public function actionIndex()
    {
        $stream = new Stream('search');
        $stream->unsetAttributes();
        if (isset($_REQUEST['Stream']))
            $stream->attributes = $_REQUEST['Stream'];
        $this->render('index', array('model' => $stream));
    }

    public function actionView($id)
    {
        $stream = self::loadModel(array('id' => $id));
        $this->render('view', array('model' => $stream));
    }

    public function actionUpdate($id)
    {
        $stream = self::loadModel(array('id' => $id));
        if (isset($_POST['Stream']))
        {
            $stream->attributes = $_POST['Stream'];
            $stream->save();
        }
        $this->render('update', array('model' => $stream));
    }

    public function actionCreate()
    {
        $stream = new Stream();
        if (isset($_POST['Stream']))
        {
            $stream->attributes = $_POST['Stream'];
            if ($stream->save())
                $this->redirect(array('/streams/view', 'id' => $stream->id));
        }
        $this->render('create', array('model' => $stream));
    }

    public function actionDelete($id)
    {
        $stream = self::loadModel(array('id' => $id));
        if (!$stream->delete())
            throw new CHttpException(500);
    }

    /**
     * @param $options
     * @return Stream
     * @throws CHttpException
     */
    public static function loadModel($options)
    {
        $model = Stream::model()->findByAttributes($options);
        if (!$model)
            throw new CHttpException(404);
        return $model;
    }

    public function filters()
    {
        return array(
            'ajaxOnly + delete',
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated users to access all actions
                'users'=>array('@'),
            ),
            array('deny'),
        );
    }

}