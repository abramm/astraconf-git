<?php

class m140718_075453_epg_correction extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
ALTER TABLE `channel`
ADD COLUMN `epgCorrection` INT(10) NOT NULL AFTER `xmlId`;
SQL;
        $this->execute($sql);
	}

	public function down()
	{
		echo "m140718_075453_epg_correction does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}