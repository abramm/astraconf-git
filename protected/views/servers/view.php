<?php
/**
 * @var $this ServersController
 * @var $model Server
 * @var $showConfig bool
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список серверов') => array('/servers/index'),
);

$this->pageHeader = Yii::t('menu','Просмотр сервера {NAME}', array('{NAME}' => $model->name));
?>
<p>
    <?php
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
            'id',
            'ip',
            'name',
            'outputsList',
            'urlInputProcesses',
        ),
    ));

    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'update',
            'caption' => Yii::t('models', 'Редактировать'),
            'url' => array('update', 'id' => $model->id),
        )
    );


    if ($showConfig)
    {
        echo CHtml::tag('h2', array(),Yii::t('menu', 'Конфигурация'));
        if ($model->haveUrlInput)
        {
            echo CHtml::tag('h3', array(), Yii::t('app', 'URL входы:'));
            for ($i = 0; $i < $model->urlInputProcesses; $i++)
                echo CHtml::textArea('astraUrlInputsConfig_'.$i, $model->getUrlInputsConfigFile($i), array('rows' => 25, 'cols' => 80, 'readonly'=>true));
        }
        if ($model->haveRelay)
        {
            echo CHtml::tag('h3', array(), Yii::t('app', 'Сборка и трансляция резервированных каналов:'));
            echo CHtml::textArea('astraRelayConfig', $model->relayConfigFile, array('rows' => 25, 'cols' => 80, 'readonly'=>true));
        }
            }
    else
    {
        $this->widget('zii.widgets.jui.CJuiButton',array(
                'buttonType'=>'link',
                'name' => 'showConfig',
                'caption' => Yii::t('models', 'Показать конфигурацию'),
                'url' => array('/servers/view', 'id' => $model->id, 'showConfig' => true),
            )
        );
    }

    ?>
</p>
