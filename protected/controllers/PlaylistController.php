<?php

class PlaylistController extends Controller
{

    const PLAYLIST_STALKER = 0;
    const PLAYLIST_STALKER_EXTENDED = 1;
    const PLAYLIST_M3U = 2;

    public function actionGet($type, $externalIp = false)
    {
        $playlistChannels = array();
        $channelsSearch = new Channel('search');
        $channelsSearch->unsetAttributes();
        $channelsSearch->with('streams');
        $channelsProvider = $channelsSearch->search();
        $channelsProvider->pagination = false;
        $allChannels = $channelsProvider->data;
        /* @var $allChannels Channel[] */
        foreach ($allChannels as $channel)
        {
            if ($channel->canCast)
            {
                $playlistChannels[] = $channel;
            }
        }
        /* @var $playlistChannels Channel[] */

        ob_start();
        switch ($type)
        {
            case self::PLAYLIST_STALKER:
            case self::PLAYLIST_STALKER_EXTENDED:
                echo 'DELETE FROM epg; DELETE FROM ch_links; DELETE FROM itv; UPDATE epg_setting SET etag = ""; ';
                if ($type == self::PLAYLIST_STALKER_EXTENDED)
                    echo 'DELETE FROM itv_tarif; DELETE FROM ssIptvIds; ';
                echo PHP_EOL;
                echo PHP_EOL;
                echo PHP_EOL;
                $num = 1;
                foreach ($playlistChannels as $channel)
                {
                    if ($channel->server->enableHttpOutput)
                        $solution = 'ffrt ';
                    elseif ($channel->server->enableUdpOutput)
                        $solution = 'rtp ';
                    else
                        $solution = '';
                    echo 'INSERT INTO `itv` VALUES(',$channel->id,', \'',$channel->name,'\', ',$num++,
                    ', 0, \'',$solution,$channel->getClientUrl($externalIp),'\', \'\', 0, 0, 1, ',$channel->genre,', 1, 0, \''.$channel->xmlId.'\', \'\', 0, 0, 0, \'\', 0, 0, 0, 0, NULL, 0, \'\', 0, \'\', \'\', \'\', \'\', '.$channel->epgCorrection.', 1, 1, 0, 1, 0, 168, 0);';
                    echo PHP_EOL;
                    echo 'INSERT INTO `ch_links` (`id`, `ch_id`, `priority`, `url`, `status`, `use_http_tmp_link`, `wowza_tmp_link`, `user_agent_filter`, `monitoring_url`, `use_load_balancing`, `changed`, `enable_monitoring`) VALUES
(',$channel->id,', ',$channel->id,', 0, \'',$solution,$channel->getClientUrl($externalIp),'\', 1, 0, 0, \'\', \'\', 0, NOW(), 0);';
                    echo PHP_EOL;
                    if ($type == self::PLAYLIST_STALKER_EXTENDED)
                    {
                        echo 'INSERT INTO `itv_tarif` (`ch_id`, `tarif`) VALUES ('.$channel->id.','.$channel->subscriptionLevel.');';
                        echo PHP_EOL;
                        if (intval($channel->ssIptvId) > 0)
                        {
                            echo 'INSERT INTO `ssIptvIds` (`ch_id`, `ssIptvId`) VALUES ('.$channel->id.','.intval($channel->ssIptvId).');';
                            echo PHP_EOL;
                        }
                    }
                    echo PHP_EOL;
                }
                break;
            case self::PLAYLIST_M3U:
                echo '#EXTM3U', PHP_EOL;
                echo '#PLAYLIST:' . Yii::app()->params['playlistName'], PHP_EOL;
                foreach ($playlistChannels as $channel)
                {
                    echo '#EXTINF:0,', $channel->name, PHP_EOL;
                    echo $channel->getClientUrl($externalIp), PHP_EOL;
                }
                break;
            default:
                throw new CHttpException(400);
        }
        $retval = ob_get_clean();
        ob_end_clean();
        return $retval;
    }

    public function actionDownload($type, $externalIp = false)
    {
        switch ($type)
        {
            case self::PLAYLIST_STALKER:
            case self::PLAYLIST_STALKER_EXTENDED:
                header ( 'Expires: Tue, 1 Jan 1980 00:00:00 GMT' );
                header ( 'Last-Modified: ' . gmdate ( 'D, d M Y H:i:s' ) . ' GMT' );
                header ( 'Cache-Control: no-store, no-cache, must-revalidate' );
                header ( 'Cache-Control: post-check=0, pre-check=0', false );
                header ( 'Pragma: no-cache' );
                header ( 'Accept-Ranges: bytes' );
                header ( 'Connection: close' );
                header ( 'Content-Transfer-Encoding: binary' );
                header ( 'Content-Disposition: attachment; filename="playlist.sql"' );
                header ( 'Content-Type: application/x-sql' );
                break;
            case self::PLAYLIST_M3U:
                header ( 'Expires: Tue, 1 Jan 1980 00:00:00 GMT' );
                header ( 'Last-Modified: ' . gmdate ( 'D, d M Y H:i:s' ) . ' GMT' );
                header ( 'Cache-Control: no-store, no-cache, must-revalidate' );
                header ( 'Cache-Control: post-check=0, pre-check=0', false );
                header ( 'Pragma: no-cache' );
                header ( 'Accept-Ranges: bytes' );
                header ( 'Connection: close' );
                header ( 'Content-Transfer-Encoding: binary' );
                header ( 'Content-Disposition: attachment; filename="playlist.m3u"' );
                header ( 'Content-Type: audio/x-mpegurl' );
                break;
        }
        echo $this->actionGet($type, $externalIp);
        Yii::app()->end();
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionView($type, $externalIp = false)
    {
        $this->render('view', array('type' => $type, 'text' => $this->actionGet($type, $externalIp)));
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated users to access all actions
                'users'=>array('@'),
            ),
            array('deny'),
        );
    }

}
