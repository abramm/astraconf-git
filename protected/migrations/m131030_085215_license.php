<?php

class m131030_085215_license extends CDbMigration
{
	public function up()
	{
        $this->execute('ALTER TABLE `channel` ADD COLUMN `isMandatory` INT(1) NOT NULL DEFAULT 0  AFTER `haveContract` , ADD COLUMN `isLicensed` INT(1) NOT NULL DEFAULT 0  AFTER `isMandatory` ;');
	}

	public function down()
	{
		echo "m131030_085215_license does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}