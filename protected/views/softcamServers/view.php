<?php
/**
 * @var $this SoftcamServersController
 * @var $model SoftcamServer
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список серверов ключей') => array('/softcamServers/index'),
);

$this->pageHeader = Yii::t('menu','Просмотр сервера ключей {NAME}', array('{NAME}' => $model->name));
?>
<p>
    <?php
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
            'name',
            'description',
            'host',
            'port',
            'username',
            'password',
            array(
                'name' => 'sendEMM',
                'value' => $model->sendEMMLabel,
            ),
            'aesKey',
        ),
    ));

    $this->widget('zii.widgets.jui.CJuiButton',array(
            'buttonType'=>'link',
            'name' => 'update',
            'caption' => Yii::t('models', 'Редактировать'),
            'url' => array('update', 'id' => $model->id),
        )
    );

    ?>
</p>
<p><?= Yii::t('models','Сервер используется следующими потоками:'); ?></p>
<p>
    <?php
    $streamSearch = new Stream('search');
    $streamSearch->unsetAttributes();
    $streamSearch->with('channel');
    $streamSearch->newSoftcamServers = $model->id;

    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider' => $streamSearch->search(),
        'columns' => array(
            'name',
            'description',
            array
            (
                'name' => 'channel.name',
                'header' => Yii::t('models', 'Название канала'),
            ),
            'casData',
        ),
    ))
    ?>
</p>
