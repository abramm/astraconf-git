<?php

class SoftcamServersController extends Controller
{
    public function actionIndex()
    {
        $softcamServer = new SoftcamServer('search');
        $softcamServer->unsetAttributes();
        if (isset($_REQUEST['SoftcamServer']))
            $softcamServer->attributes = $_REQUEST['SoftcamServer'];
        $this->render('index', array('model' => $softcamServer));
    }

    public function actionView($id)
    {
        $softcamServer = self::loadModel(array('id' => $id));
        $this->render('view', array('model' => $softcamServer));
    }

    public function actionUpdate($id)
    {
        $softcamServer = self::loadModel(array('id' => $id));
        if (isset($_POST['SoftcamServer']))
        {
            $softcamServer->attributes = $_POST['SoftcamServer'];
            $softcamServer->save();
        }
        $this->render('update', array('model' => $softcamServer));
    }

    public function actionCreate()
    {
        $server = new SoftcamServer();
        if (isset($_POST['SoftcamServer']))
        {
            $server->attributes = $_POST['SoftcamServer'];
            if ($server->save())
                $this->redirect(array('/softcamServers/view', 'id' => $server->id));
        }
        $this->render('create', array('model' => $server));
    }

    public function actionDelete($id)
    {
        $softcamServer = self::loadModel(array('id' => $id));
        if (!$softcamServer->delete())
            throw new CHttpException(500);
    }

    public static function loadModel($options)
    {
        $model = SoftcamServer::model()->findByAttributes($options);
        if (!$model)
            throw new CHttpException(404);
        return $model;
    }

    public function filters()
    {
        return array(
            'ajaxOnly + delete',
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated users to access all actions
                'users'=>array('@'),
            ),
            array('deny'),
        );
    }

}