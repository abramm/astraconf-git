<?php
/* @var $this TranspodersController */
/* @var $model DvbSOptions */
/* @var $form CActiveForm */
?>

<div class="form">


    <h2><?= Yii::t('menu', 'Опции DVB-S/S2');?></h2>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'frequency'); ?>
        <?php echo $form->textField($model,'frequency'); ?>
        <?php echo $form->error($model,'frequency'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'polarization'); ?>
        <?php echo $form->textField($model,'polarization'); ?>
        <?php echo $form->error($model,'polarization'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'symbolrate'); ?>
        <?php echo $form->textField($model,'symbolrate'); ?>
        <?php echo $form->error($model,'symbolrate'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'lof1'); ?>
        <?php echo $form->textField($model,'lof1'); ?>
        <?php echo $form->error($model,'lof1'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'lof2'); ?>
        <?php echo $form->textField($model,'lof2'); ?>
        <?php echo $form->error($model,'lof2'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'slof'); ?>
        <?php echo $form->textField($model,'slof'); ?>
        <?php echo $form->error($model,'slof'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'lnb_sharing'); ?>
        <?php echo $form->checkBox($model,'lnb_sharing'); ?>
        <?php echo $form->error($model,'lnb_sharing'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'diseqc'); ?>
        <?php echo $form->textField($model,'diseqc'); ?>
        <?php echo $form->error($model,'diseqc'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'tone'); ?>
        <?php echo $form->checkBox($model,'tone'); ?>
        <?php echo $form->error($model,'tone'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'rolloff'); ?>
        <?php echo $form->textField($model,'rolloff'); ?>
        <?php echo $form->error($model,'rolloff'); ?>
    </div>

</div><!-- form -->