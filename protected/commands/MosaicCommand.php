<?php

class MosaicCommand extends CConsoleCommand{

    public function actionShotAll($threads = 4)
    {
        $channelSearch = new Channel('search');
        $channelSearch->unsetAttributes();
        $channelSearch->canCastSearch = 1;
        $provider = $channelSearch->search();
        $provider->pagination = false;
        $channels = $provider->getData();
        shuffle($channels);
        $channelsParts = array_chunk($channels, ceil($provider->itemCount / $threads));
        $pids = array();
        Yii::app()->getDb()->setActive(false);

        foreach ($channelsParts as $channels)
        {
            $pid = pcntl_fork();
            if ($pid == -1) {
                die('could not fork');
            } else if ($pid) {
                // we are the parent
//                echo "Process ", $pid, " started, took ",count($channels), " channels.", PHP_EOL;
                $pids[] = $pid;
            } else {
                Yii::app()->getDb()->setActive(true);
                usleep(rand(0,1000000));

                foreach ($channels as $channel)
                {
                    /* @var $channel Channel */
                    $channel->getMosaicImageUrl(true, true);
                }
                break;
            }
        }
        Yii::app()->getDb()->setActive(true);
        foreach ($pids as $pid)
        {
            $status = null;
            pcntl_waitpid($pid, $status);
        }
    }

    public function actionShotOne($id)
    {
        $channel = Channel::model()->findByPk($id);
        /* @var $channel Channel */
        if ($channel)
            echo $channel->getMosaicImageUrl(true, true), PHP_EOL;
        else
            echo 'Channel not found', PHP_EOL;
    }

}