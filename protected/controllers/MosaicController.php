<?php

class MosaicController extends Controller{

    public function actionIndex($level = null)
    {
        MonitoringEvent::cleanup();
        $this->render('index', array('level' => $level));
    }

    public function actionBox($id, $forceRefresh = 0)
    {
        $channel = Channel::model()->findByPk($id);
        /* @var $channel Channel */
        if (!$channel)
            throw new CHttpException(404);
        if ($forceRefresh)
            $channel->refreshMosaicImage();
        $this->renderPartial('_box', array('channel' => $channel, 'mayRefresh' => true));
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated users to access all actions
                'users'=>array('@'),
            ),
            array('deny'),
        );
    }

}
