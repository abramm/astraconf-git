<?php
/**
 * @var $softcamServer SoftcamServer
 * @var $stream Stream
 * @var $name string
 */
?>
<?= $name ?> = newcamd({ -- <?= $softcamServer->name ?> --
    name = "<?= $name ?>" ,
    host = "<?= $softcamServer->host ?>", port = <?= $softcamServer->port ?>,
    user = "<?= $softcamServer->username ?>", pass = "<?= $softcamServer->password ?>",
    key = "<?= $softcamServer->aesKey ?>", disable_emm = <?= $softcamServer->sendEMM ? 'false' : 'true' ?>,
<?php
if ($stream->casData)
{
    ?>
    cas_data = "<?= $stream->casData ?>",
<?php
}
?>
})

