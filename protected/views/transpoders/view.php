<?php
/**
 * @var $this TranspodersController
 * @var $model DvbTranspoder
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список трансподеров') => array('/transpoders/index'),
);

$this->pageHeader = Yii::t('menu','Просмотр трансподера {NAME}', array('{NAME}' => $model->description));
?>
<p>
    <?php
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
            'description',
            'satellite',
            'adapterLink:html',
            'type',
        ),
    ));
    ?>
</p>
<?php
if ($model->streamsCount > 0)
{
?>
<h2><?= Yii::t('models','Потоки на несущей'); ?></h2>
<p>
    <?php
    $streamSearch = new Stream('search');
    $streamSearch->with('channel');
    $streamSearch->unsetAttributes();
    $streamSearch->dvbTranspoderId = $model->id;

    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider' => $streamSearch->search(),
        'columns' => array(
            'name',
            'channelLink:html',
            'pnr',
            array(
                'name' => 'isEncoded',
                'value' => '$data->isEncodedLabel',
                'filter' => Stream::getIsEncodedList(),
            ),
        ),
    ))
    ?>
</p>
<?php
}
?>