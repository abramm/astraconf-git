<?php

/**
 * This is the model class for table "adapter".
 *
 * The followings are the available columns in table 'adapter':
 * @property string $id
 * @property string $serverId
 * @property string $num
 * @property string $device
 * @property string $mac
 *
 * The followings are the available model relations:
 * @property Server $server
 * @property DvbTranspoder $dvbTranspoder
 *
 * @property string $displayName
 * @property string $serverLink
 * @property string $inUse
 * @property string $configFile
 * @property AstraConfig $astraConfig
 */
class Adapter extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'adapter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('serverId', 'required'),
			array('serverId, num, device', 'length', 'max'=>10),
			array('mac', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, serverId, num, device, mac', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'server' => array(self::BELONGS_TO, 'Server', 'serverId'),
			'dvbTranspoder' => array(self::HAS_ONE, 'DvbTranspoder', 'adapterId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'serverId' => Yii::t('models', 'Сервер'),
			'serverLink' => Yii::t('models', 'Сервер'),
			'num' => Yii::t('models', 'Номер адаптера в системе'),
			'device' => Yii::t('models', 'Номер устройства в DVB-адаптере'),
			'mac' => Yii::t('models', 'MAC-адрес'),
			'inUse' => Yii::t('models', 'Используется'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $tableAlias = $this->getTableAlias(true);

		$criteria->compare($tableAlias.'.`id`',$this->id);
		$criteria->compare($tableAlias.'.`serverId`',$this->serverId);
		$criteria->compare($tableAlias.'.`num`',$this->num,true);
		$criteria->compare($tableAlias.'.`device`',$this->device,true);
		$criteria->compare($tableAlias.'.`mac`',$this->mac,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize' => 40,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Adapter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getDisplayName()
    {
        return Yii::t('models', '{NUM} на {SERVER}', array('{NUM}' => $this->num, '{SERVER}' => $this->server->name));
    }

    public function defaultScope()
    {
        return array(
            'with' => 'server',
        );
    }

    public function getServerLink()
    {
        if ($this->isNewRecord)
            return null;
        return CHtml::link($this->server->name, array('/servers/view', 'id' => $this->serverId));
    }

    public function getInUse()
    {
        if ($this->dvbTranspoder)
            return $this->dvbTranspoder->description;
        else
            return null;
    }

    public function getConfigFile()
    {
        return $this->astraConfig->getConfigurationScript();
//        return Yii::app()->controller->renderPartial('//adapters/_astraConfig', array('adapter' => $this), true);
    }

    public function delete()
    {
        $retval = false;
        $useTransaction = !$this->dbConnection->currentTransaction;
        if ($useTransaction)
            $transaction = $this->dbConnection->beginTransaction();
        try
        {
            if ($this->dvbTranspoder && !$this->dvbTranspoder->delete())
                throw new CException('Error deleting DVB Transpoder '.$this->dvbTranspoder->id);
            $retval = parent::delete();
            if ($useTransaction)
                $transaction->commit();
        }
        catch(Exception $e)
        {
            Yii::log('Error deleting stream '.$this->id.', caught exception: '.$e->getMessage(), CLogger::LEVEL_ERROR);
            if ($useTransaction)
                $transaction->rollback();
        }
        return $retval;
    }

    /**
     * @return AstraConfig
     */
    public function getAstraConfig()
    {
        $conf = new AstraConfig();
        $conf->confName = 'a'.$this->num;
        foreach ($this->dvbTranspoder->streams as $stream)
        {
            if ($stream->canCast)
                $conf->addChannel($stream->channel, array($stream), $stream->name);
        }
        return $conf;
    }
}
