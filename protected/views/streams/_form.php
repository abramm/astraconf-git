<?php
/* @var $this StreamController */
/* @var $model Stream */
/* @var $form CActiveForm */

$channelSortCriteria = new CDbCriteria();
$channelSortCriteria->order = 'name ASC';
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'stream-_form-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note"><?= Yii::t('models', 'Поля с {*} обязательны к заполнению', array('{*}' => '<span class="required">*</span>')); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'serverId'); ?>
		<?php echo $form->dropDownList($model,'serverId',CHtml::listData(Server::model()->findAll(),'id','name')); ?>
		<?php echo $form->error($model,'serverId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'channelId'); ?>
		<?php echo $form->dropDownList($model,'channelId', CHtml::listData(Channel::model()->findAll($channelSortCriteria), 'id', 'name')); ?>
		<?php echo $form->error($model,'channelId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type', Stream::getTypesList()); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'isEncoded'); ?>
		<?php echo $form->dropDownList($model,'isEncoded', Stream::getIsEncodedList()); ?>
		<?php echo $form->error($model,'isEncoded'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description'); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url'); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dvbTranspoderId'); ?>
		<?php echo $form->dropDownList($model,'dvbTranspoderId', CHtml::listData(DvbTranspoder::model()->findAll(),'id', 'description'), array('prompt' => '')); ?>
		<?php echo $form->error($model,'dvbTranspoderId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'priority'); ?>
		<?php echo $form->textField($model,'priority'); ?>
		<?php echo $form->error($model,'priority'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pnr'); ?>
		<?php echo $form->textField($model,'pnr'); ?>
		<?php echo $form->error($model,'pnr'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'useBackup'); ?>
        <?php echo $form->checkBox($model,'useBackup'); ?>
        <?php echo $form->error($model,'useBackup'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'filter'); ?>
        <?php echo $form->textField($model,'filter'); ?>
        <?php echo $form->error($model,'filter'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'biss'); ?>
		<?php echo $form->textField($model,'biss'); ?>
		<?php echo $form->error($model,'biss'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'casData'); ?>
		<?php echo $form->textField($model,'casData'); ?>
		<?php echo $form->error($model,'casData'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model, 'newSoftcamServers') ?>
        <?php
        $selectedServers = $model->softcamServers;
        $htmlOptions = array('multiple' => true, 'size' => 10, 'empty' => '');
        $model->oldSoftcamServers = array();
        foreach ($selectedServers as $server)
        {
            $htmlOptions['options'][$server->id] = array('selected' => true,);
            $model->oldSoftcamServers[$server->id] = $server->id;
            echo $form->hiddenField($model, 'oldSoftcamServers['.$server->id.']');
        }
        $listData = CHtml::listData(SoftcamServer::model()->findAll(), 'id', 'name');
        echo $form->dropDownList($model,'newSoftcamServers',$listData, $htmlOptions);
        ?>
        <?php echo $form->error($model,'newSoftcamServers'); ?>
    </div>

	<div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('app', 'Сохранить')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->