<?php

/**
 * This is the model class for table "server".
 *
 * The followings are the available columns in table 'server':
 * @property string $id
 * @property string $name
 * @property string $ip
 * @property string $enableHttpOutput
 * @property string $enableUdpOutput
 * @property string $keepHttpActive
 * @property string $externalIp
 * @property string $urlInputProcesses
 *
 * The followings are the available model relations:
 * @property Adapter[] $adapters
 * @property Channel[] $channels
 * @property Stream[] $streams
 *
 * @property string $relayConfigFile
 * @property bool $haveUrlInput
 * @property bool $haveRelay
 * @property string $outputsList
 */
class Server extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'server';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, ip', 'required'),
			array('name', 'length', 'max'=>255),
			array('ip, externalIp, urlInputProcesses', 'length', 'max'=>45),
            array('enableHttpOutput, enableUdpOutput, keepHttpActive', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, ip, enableHttpOutput, enableUdpOutput, keepHttpActive, externalIp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'adapters' => array(self::HAS_MANY, 'Adapter', 'serverId'),
            'channels' => array(self::HAS_MANY, 'Channel', 'serverId'),
			'streams' => array(self::HAS_MANY, 'Stream', 'serverId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('models', 'Имя'),
			'ip' => Yii::t('models', 'IP адрес'),
            'enableHttpOutput' => Yii::t('models', 'HTTP вывод'),
            'enableUdpOutput' => Yii::t('models', 'UDP вывод'),
            'keepHttpActive' => Yii::t('models', 'Не отключать неактивные вводы (для HTTP вывода)'),
            'externalIp' => Yii::t('models', 'Внешний IP (для HTTP серверов за NAT)'),
            'outputsList' => Yii::t('models', 'Выводы'),
            'urlInputProcesses' => Yii::t('models', 'Количество URL Input процесов'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('ip',$this->ip,true);
        $criteria->compare('enableHttpOutput',$this->enableHttpOutput);
        $criteria->compare('enableUdpOutput',$this->enableUdpOutput);
        $criteria->compare('keepHttpActive',$this->keepHttpActive);
        $criteria->compare('externalIp',$this->externalIp);
        $criteria->compare('urlInputProcesses',$this->urlInputProcesses);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Server the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getRelayConfigFile()
    {
        return Yii::app()->controller->renderPartial('//servers/_relayConfig', array('server' => $this), true);
    }

    public function getUrlInputsConfigFile($num)
    {
        return Yii::app()->controller->renderPartial('//servers/_urlConfig', array('server' => $this, 'num' => $num), true);
    }

    public function getHaveUrlInput()
    {
        foreach ($this->streams as $stream)
        {
            if ($stream->type == Stream::TYPE_URL && $stream->canCast)
                return true;
        }
        return false;
    }

    public function getHaveRelay()
    {
        foreach ($this->channels as $channel)
        {
            if ($channel->canCast && !$channel->directCast)
                return true;
        }
        return false;
    }

    public function getOutputsList()
    {
        $outputs = array();
        if ($this->enableHttpOutput)
            $outputs[] = 'HTTP';
        if ($this->enableUdpOutput)
            $outputs[] = 'UDP';
        if (count($outputs) == 0)
            return Yii::t('gen', 'Нет');
        return implode(', ', $outputs);
    }

    public function delete()
    {
        $retval = false;
        $useTransaction = !$this->dbConnection->currentTransaction;
        if ($useTransaction)
            $transaction = $this->dbConnection->beginTransaction();
        try
        {
            foreach ($this->adapters as $adapter)
            {
                if (!$adapter->delete())
                    throw new CException('Error deleting adapter '.$adapter->id);
            }
            foreach ($this->channels as $channel)
            {
                if (!$channel->delete())
                    throw new CException('Error deleting channel '.$channel->id);
            }
            foreach ($this->streams as $stream)
            {
                if (!$stream->delete())
                    throw new CException('Error deleting stream '.$stream->id);
            }
            $retval = parent::delete();
            if ($useTransaction)
                $transaction->commit();
        }
        catch(Exception $e)
        {
            Yii::log('Error deleting channel '.$this->id.', caught exception: '.$e->getMessage(), CLogger::LEVEL_ERROR);
            if ($useTransaction)
                $transaction->rollback();
        }
        return $retval;
    }
}
