<?php

class AstraConfig {

    public $eventRequestUrl;
    public $eventRequestInterval;
    public $controlServerUrl;
    public $confName;

    /**
     * @var AstraConfigNewcamd[]
     */
    public $newcamdConnections = [];
    /**
     * @var AstraConfigDvbTune[]
     */
    public $dvbTunes = [];
    /**
     * @var AstraConfigChannel[]
     */
    public $channels = [];

    private $identationLevel = 0;
    private $confStr = '';

    public function getConfigurationScript()
    {
        $this->confStr = '';
        $this->identationLevel = 0;
        $this->addConfShebang();
        $this->addConfNewline();
        $this->addConfMonitoringParams();
        $this->addConfNewline();
        $this->addConfString('pidfile("/var/run/astra_'.$this->confName.'.pid")');
        $this->addConfString('log.set({filename = "/var/log/astra/'.$this->confName.'.log"})');
        $this->addConfNewline();
        $this->addConfNewcamdConnections();
        $this->addConfNewline();
        $this->addConfDvbTunes();
        $this->addConfNewline();
        $this->addConfChannels();

        return $this->confStr;

    }

    protected function addConfShebang()
    {
        $this->addConfString('#!/usr/bin/env astra');
    }

    protected function addConfMonitoringParams()
    {
        $this->addSection('if (init_all ~= nil) then',[
            'event_request = "'. Yii::app()->getRequest()->getHostInfo().Yii::app()->urlManager->createUrl('/monitoring/event').'"',
            'event_request_interval = 30',
            'init_all()',
        ],'end', Yii::t('app', 'мониторинг'));
    }

    protected function addConfString($val, $comment = null, $ident = 0)
    {
        $lines = explode("\n", $val);
        if (count($lines) > 1)
        {
            $insertedComment = false;
            foreach($lines as $line)
            {
                if ($insertedComment)
                    $this->addConfString($line);
                else
                    $this->addConfString($line, $comment);
                $insertedComment = true;
            }
        }
        else
        {
            $val = trim($val);
            if (strlen($val) > 0)
            {
                $this->confStr.=str_repeat(' ', ($this->identationLevel + $ident) * 4);
                $this->confStr.=$val;
                if ($comment)
                    $this->confStr.=' -- '.$comment;
                $this->confStr.=PHP_EOL;
            }
        }
    }

    protected function addSection($start, $content, $end, $comment = null)
    {
        $this->addConfString($start, $comment);
        $this->identationLevel++;
        if (is_array($content))
        {
            $this->addSubsection($content);
        }
        else
            $this->addConfString($content, null, 1);
        $this->identationLevel--;
        $this->addConfString($end);
    }

    protected function addSubsection($val)
    {
        if (is_array($val))
        {
            if (array_key_exists('start', $val) && array_key_exists('end', $val) && array_key_exists('content', $val))
            {
                $this->identationLevel++;
                if (array_key_exists('comment', $val))
                    $comment = $val['comment'];
                $this->addSection($val['start'], $val['content'], $val['end'], $comment);
                $this->identationLevel--;
            }
            else
            {
                foreach($val as $row)
                    $this->addSubsection($row);
            }
        }
        else
            $this->addConfString($val, null, 1);
    }

    protected function addConfNewcamdConnections()
    {
        foreach ($this->newcamdConnections as $connection)
        {
            $val = $connection->asConfArray();
            $this->addSection($val['start'], $val['content'], $val['end'], $val['comment']);
            $this->addConfNewline();
        }
    }

    protected function addConfDvbTunes()
    {
        foreach ($this->dvbTunes as $tune)
        {
            $val = $tune->asConfArray();
//            var_dump($val);
//            exit();
            $this->addSection($val['start'], $val['content'], $val['end'], $val['comment']);
            $this->addConfNewline();
        }
    }

    protected function addConfChannels()
    {
        foreach ($this->channels as $channel)
        {
            $val = $channel->asConfArray();
            $this->addSection($val['start'], $val['content'], $val['end'], $val['comment']);
            $this->addConfNewline();
        }
    }

    protected function addConfNewline()
    {
        $this->confStr.=PHP_EOL;
    }

    /**
     * @param $name string
     * @param $softcamServer SoftcamServer
     * @return AstraConfig self
     */
    public function addNewcamdConf($name, $softcamServer)
    {
        if (!array_key_exists($name, $this->newcamdConnections));
        {
            $this->newcamdConnections[$name] = new AstraConfigNewcamd(
                $name,
                $softcamServer->name,
                $softcamServer->host,
                $softcamServer->port,
                $softcamServer->username,
                $softcamServer->password,
                $softcamServer->aesKey,
                !$softcamServer->sendEMM
            );
        }
        return $this;
    }

    /**
     * @param $name string
     * @param $adapter Adapter
     * @return AstraConfig self
     * @throws Exception
     */
    public function addDvbTune($name, $adapter)
    {
        $transpoder = $adapter->dvbTranspoder;
        if (!array_key_exists($name, $this->dvbTunes))
        {
            $tune = new AstraConfigDvbTune($name);
            $tune->comment = $adapter->displayName;
            if ($adapter->mac && Yii::app()->params['useMACAddressInConfig'])
            {
                $tune->params['mac'] = $adapter->mac;
            }
            else
            {
                $tune->params['adapter'] = $adapter->num;
            }
            $tune->params['device'] = $adapter->device;
            if ($transpoder->budget !== null)
                $tune->params['budget'] = (bool) $transpoder->budget;
            if ($transpoder->modulation)
                $tune->params['modulation'] = $transpoder->modulation;
            if ($transpoder->fec)
                $tune->params['fec'] = $transpoder->fec;
            $tune->params['buffer_size'] = Yii::app()->params['DVBBufferSize'];
            $tune->params['type'] = $transpoder->type;

            switch ($transpoder->type)
            {
                case 'S':
                case 'S2':
                    $options = $transpoder->dvbSOptions;
                    $tune->params = CMap::mergeArray($tune->params,
                        [
                            'tp' => $options->frequency.':'.$options->polarization.':'.$options->symbolrate,
                            'lnb' => $options->lof1.':'.$options->lof2.':'.$options->slof,
                        ]);
                    if ($options->lnb_sharing)
                        $tune->params['lnb_sharing'] = true;
                    if ($options->diseqc)
                        $tune->params['diseqc'] = intval($options->diseqc);
                    if ($options->tone)
                        $tune->params['tone'] = true;
                    if ($options->rolloff)
                        $tune->params['rolloff']  = intval($options->rolloff);
                    break;
                case 'T':
                case 'T2':
                    $options = $transpoder->dvbTOptions;
                    $tune->params = CMap::mergeArray($tune->params,
                        [
                            'frequency' => $options->frequency,
                        ]);
                    if ($options->bandwidth)
                        $tune->params['bandwidth'] = $options->bandwidth;
                    if ($options->guardinterval)
                        $tune->params['guardinterval'] = $options->guardinterval;
                    if ($options->transmitmode)
                        $tune->params['transmitmode'] = $options->transmitmode;
                    if ($options->hierarchy)
                        $tune->params['hierarchy'] = $options->hierarchy;
                    break;
                case 'C':
                    $options = $transpoder->dvbCOptions;
                    $tune->params = CMap::mergeArray($tune->params,
                        [
                            'frequency' => $options->frequency,
                            'symbolrate' => $options->symbolrate,
                        ]);
                    break;
                default:
                    throw new Exception('Unknown DVB type "'.$transpoder->type.'"');
            }

            $this->dvbTunes[$name] = $tune;
        }
        return $this;
    }

    /**
     * @param $channel Channel
     * @param $streams Stream[]
     * @param $comment string
     * @return $this
     */
    public function addChannel($channel, $streams, $comment)
    {
        $channelConf = new AstraConfigChannel($this, $channel, $comment);
        foreach($streams as $stream)
            $channelConf->addInput($stream);
        if ($stream->channel->directCast && $stream->useBackup)
            $channelConf->addFallback();
        if ($channel->directCast)
        {
            if ($channel->server->enableHttpOutput)
                $channelConf->outputUrls[] = $channel->outputHttpUrl;
            if ($channel->server->enableUdpOutput)
                $channelConf->outputUrls[] = $channel->outputUdpUrl;
        }
        else
            $channelConf->outputUrls = [$stream->internalCastUrl];
        $this->channels[] = $channelConf;
        return $this;
    }
}