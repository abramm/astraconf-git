<?php $this->widget('zii.widgets.CMenu',array(
    'items'=>array(
        array('label'=>Yii::t('menu','Серверы'), 'url'=>array('/servers/index'), 'visible' => !Yii::app()->user->isGuest),
        array('label'=>Yii::t('menu','Каналы'), 'url'=>array('/channels/index'), 'visible' => !Yii::app()->user->isGuest),
        array('label'=>Yii::t('menu','Потоки'), 'url'=>array('/streams/index'), 'visible' => !Yii::app()->user->isGuest),
        array('label'=>Yii::t('menu','Серверы ключей'), 'url'=>array('/softcamServers/index'), 'visible' => !Yii::app()->user->isGuest),
        array('label'=>Yii::t('menu','DVB трансподеры'), 'url'=>array('/transpoders/index'), 'visible' => !Yii::app()->user->isGuest),
        array('label'=>Yii::t('menu','DVB адаптеры'), 'url'=>array('/adapters/index'), 'visible' => !Yii::app()->user->isGuest),
        array('label'=>Yii::t('menu','Плейлист'), 'url'=>array('/playlist/index'), 'visible' => !Yii::app()->user->isGuest),
        array('label'=>Yii::t('menu','Мозаика каналов'), 'url'=>array('/mosaic'), 'visible' => !Yii::app()->user->isGuest),
        array('label'=>Yii::t('menu','Вход'), 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
        array('label'=>Yii::t('menu','Выход ({USER})', array('{USER}' => Yii::app()->user->name)), 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
    ),
));