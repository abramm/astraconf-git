<?php

class m140815_105543_server_external_ip extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
ALTER TABLE `server`
ADD COLUMN `externalIp` VARCHAR(45) NULL DEFAULT NULL AFTER `keepHttpActive`
SQL;
        $this->execute($sql);
	}

	public function down()
	{
		echo "m140815_105543_server_external_ip does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}