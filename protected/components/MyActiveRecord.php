<?php

class MyActiveRecord extends CActiveRecord {

    private $_cachedAttributes = array();

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function save($runValidation = true, $attributes = null) {
        $result = parent::save($runValidation, $attributes);
        if (!$result) {
            Yii::log('Could not save AR record ' . get_class($this) . ' ID ' . var_export($this->primaryKey, true) . ': ' . var_export($this->errors, true), CLogger::LEVEL_ERROR);
        }
        return $result;
    }

    public function cachedAttributes()
    {
        return array();
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that cached attributes can be accessed like properties.
     * @param string $name property name
     * @return mixed property value
     */
    public function __get($name)
    {
        if(isset($this->_cachedAttributes[$name]))
            return $this->getCachedAttribute($name);
        else
            return parent::__get($name);
    }

    public function __construct($scenario='insert')
    {
        $this->_cachedAttributes = $this->cachedAttributes();
        return parent::__construct($scenario);
    }

    public function getCachedAttribute($name)
    {
        $cache = Yii::app()->cache;
        if (is_array($this->primaryKey))
            $cacheId = 'cachedAttribute_'.get_class($this).'_'.$name.'_'.implode('/',$this->primaryKey);
        else
            $cacheId = 'cachedAttribute_'.get_class($this).'_'.$name.'_'.$this->primaryKey;
        $value = $cache->get($cacheId);
        if ($value === false)
        {
            Yii::trace('Could not get cached attribute '.get_class($this).'::'.$name.', calculating.','MyActiveRecord');
            $value = parent::__get($name);
            $cacheTimeRaw = $this->_cachedAttributes[$name]['expire'];
            if (is_numeric($cacheTimeRaw))
                $cacheTime = $cacheTimeRaw;
            elseif (is_array($cacheTimeRaw))
                $cacheTime = rand($cacheTimeRaw[0], $cacheTimeRaw[1]);
            else
                $cacheTime = false;
            if ($cacheTime)
            {
                $cache->set($cacheId, $value, $cacheTime);
            }
        }
        return $value;
    }

    /**
     * @param $val bool Value
     * @return string HTML representaion of a string. Yes, no, etc
     */
    public static function boolStringHtml($val, $spanId = null)
    {
        if($val){
            return '<span '.($spanId ? 'id="'.$spanId.'" ' : '').'style="color: green;">'.Yii::t('gen','Да').'</span>';
        } else {
            if (is_null($val))
            {
                return '<span '.($spanId ? 'id="'.$spanId.'" ' : '').'style="color: grey;">'.Yii::t('gen','Неизвестно').'</span>';
            }
            else
            {
                return '<span '.($spanId ? 'id="'.$spanId.'" ' : '').'style="color: red;">'.Yii::t('gen','Нет').'</span>';
            }
        }
    }

    public static function boolStringPossibleValues()
    {
        return array(
            0 => Yii::t('gen', 'Нет'),
            1 => Yii::t('gen', 'Да'),
        );
    }

    public static function genRandomString($len)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = '';
        for ($i = 0; $i < $len; $i++) {
            $code .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $code;
    }

}