<?php
/**
 * @var $this StreamsController
 * @var $model Stream
 */

$this->breadcrumbs=array(
    Yii::t('menu', 'Список потоков') => array('/streams/index'),
);

$this->pageHeader = Yii::t('menu','Редактирование потока {NAME}', array('{NAME}' => $model->name));
?>
<p>
    <?php
    $this->renderPartial('_form', array('model' => $model));
    ?>
</p>
